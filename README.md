## Installing

Run `npm install` and `npm run build` before anything to install all NPM dependencies and build all typescript files.
Create .env file will all the necessary environment variables.

## .env configuration

Configure .env file at project root with environment variables.

Parameter | Description
--- | ---
NODE_ENV | Set the node environment ('production' or 'development')
SERVER_POST | Server's listening port
SERVER_SESSION_SECRET | Secret to initialize the server session with
DATABASE_HOST | The host machine hosting the database
DATABASE_PORT | The port the database is listening on
DATABASE_USER | The username to login to the database with
DATABASE_PASS | The password to login to the database as
DATABASE_NAME | The name of the database to use
MAILER_HOST | The host of the SMTP server
MAILER_PORT | The port of the SMTP server
MAILER_USER | The user of the SMTP server
MAILER_PASS | The password of the SMTP server

## NPM commands

Run with `npm run <command> -- [options]`.

Command | Description
--- | ---
start | Start the project (launch the server)
build | Run all build processes
test | Run the tests for both server and client
server-built | Run all build processes for the server
server-watch | Run all build processes for the server in watch mode
server-test | Run server tests
client-build | Run all build processes for the client
client-watch | Run all build processes for the client in watch mode
client-test | Run client tests
serve | Launch the server
serve-watch | Launch the server in watch mode (restart after new changes)
debug | Build everything and start the project in debug mode (watch server changes, build changes)
debug-watch | Start project in debug mode (watch server changes, build changes). Differs from 'debug' in that 'debug' first builds everything and then starts the server

db-build | Build the database and populate with basic items (wipes all data)
db-pop | Populate the database with generated values

api-doc | Build the documentation for the server api

## basic-authentication

To enable basic authentication for the server create a .basicauth file at the root directory.
This file should contain all account credentials, one account per line.
Each line should be in the form username,password.

For example the following file:

`
personzone,personzone
pedro,pass
`

creates two accounts usable for basic authentication, one with username "personzone" and password "personzone" and the other with username "pedro" and password "pass".

Note that the server must be restarted for the changes to .basicauth to take effect

## Project folder structure

Folder | Description
--- | ---
dist | Folder containing all built files
node_modules | node_modules folder
public | Public files accessable externally from the server
docs | Folder containing documentation and other relevant files
data | Folder containing random data files
src | Folder containing all files that need to be built
src.client | Folder containing all files that pertrain to the client
src.client.js | JS source files for the client
src.server | Folder containing all files that pertrain to the server
src.server.config | Complex configuration files for the server, such as passport authentication configuration
src.server.controllers | Server routes
src.server.data | Data needed for the server and generation scripts
src.server.errors | Server specific error definition
src.server.middleware | Middleware for expressjs
src.server.models | Data models
src.server.routines | Routine scripts for the server (e.g. database-build)
src.server.types | Typescript type definitions for the server
src.server.util | Util methods for the server
src.server.server.ts | Starting point of server application