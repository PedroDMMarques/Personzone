define({ "api": [
  {
    "type": "delete",
    "url": "/users/:qpurl/email/:email",
    "title": "Delete email",
    "name": "Delete_email",
    "group": "Emails",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "qpurl",
            "description": "<p>The personal url of the user to delete the email of</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "qemail",
            "description": "<p>The &quot;name&quot; of the email to delete</p>"
          }
        ]
      }
    },
    "examples": [
      {
        "title": "Response",
        "content": "HTTP/1.1 200 OK",
        "type": "json"
      }
    ],
    "version": "0.0.0",
    "filename": "src/server/controllers/api/users/email.ts",
    "groupTitle": "Emails",
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "EmailIsPrimaryError",
            "description": "<p>The action cannot be completed because the email is set as primary</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "EmailNotFoundError",
            "description": "<p>The email could not be found</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "UnauthorizedError",
            "description": "<p>You are not authorized to make that request</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "EmailIsPrimaryError",
          "content": "HTTP/1.1 400 Bad request\n{\n\terror: \"EmailIsPrimaryError\",\n\tmessage: \"Email 'a@b.c' is set as primary\"\n}",
          "type": "json"
        },
        {
          "title": "EmailNotFoundError",
          "content": "HTTP/1.1 404 Not found\n{\n\terror: \"EmailNotFoundError\",\n\tmessage: \"Email 'a@b.c' was not found\"\n}",
          "type": "json"
        },
        {
          "title": "UnauthorizedError",
          "content": "HTTP/1.1 401 Unauthorized\n{\n\terror: \"UnauthorizedError\",\n\tmessage: \"You are not authorized to make that request\"\n}",
          "type": "json"
        }
      ]
    }
  },
  {
    "type": "post",
    "url": "/user/:qpurl/password",
    "title": "Change password",
    "name": "Change_password",
    "group": "Users",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "qpurl",
            "description": "<p>The personal url of the user to change the password of</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "password",
            "description": "<p>The new password</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "oldPassword",
            "description": "<p>The previous password</p>"
          }
        ]
      }
    },
    "examples": [
      {
        "title": "Response",
        "content": "HTTP/1.1 200 OK",
        "type": "json"
      }
    ],
    "version": "0.0.0",
    "filename": "src/server/controllers/api/users/index.ts",
    "groupTitle": "Users",
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "UnauthorizedError",
            "description": "<p>You are not authorized to make that request</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "UserNotFoundError",
            "description": "<p>The user could not be found</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "UnauthorizedError",
          "content": "HTTP/1.1 401 Unauthorized\n{\n\terror: \"UnauthorizedError\",\n\tmessage: \"You are not authorized to make that request\"\n}",
          "type": "json"
        },
        {
          "title": "UserNotFoundError",
          "content": "HTTP/1.1 404 Not found\n{\n\terror: \"UserNotFoundError\",\n\tmessage: \"User 'pedro123' was not found\"\n}",
          "type": "json"
        }
      ]
    }
  }
] });
