module.exports = [
	"incredible", "passionate", "defiant", "brave", "compassionate", "entertainer",
	"beautiful", "destined", "flawed", "disturbed", "interesting", "interested",
	"discovery", "indeed", "something", "another", "uncovering",
	"brilliant", "genius", "intellectual", "funny", "charming", "killjoy"
];