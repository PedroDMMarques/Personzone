import crypto from "crypto";

/**
 * Generate a random password salt
 * @returns {string} generated password salt
 */
export function generatePasswordSalt(): string {
	return crypto.randomBytes(16).toString("hex");
}

/**
 * Generate a random email verification hash
 * @returns {string} generated email verification hash
 */
export function generateEmailVerificationHash(): string {
	return crypto.randomBytes(16).toString("hex");
}

/**
 * Hash a password with a salt value
 * @param {string} password The password text
 * @param {string} salt The salt
 * @returns {string} hashed password
 */
export function hashPassword(password: string, salt: string): string {
	const hash = crypto.createHash("sha256");
	hash.update(password + salt);
	return hash.digest("hex");
}