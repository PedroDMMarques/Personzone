import _ from "lodash";
import validator from "validator";

/**
 * Check if a string is a valid email
 * @param {string} email The string to verify
 * @returns {boolean} true if the string is an email, false otherwise
 */
export function isEmail(email: string): boolean {
	return validator.isEmail(email);
}

/**
 * Check if a string is a valid purl
 * @todo Pretty obvious
 * @param {string} purl The string to verify
 * @returns {boolean} true if the string is a purl, false otherwise
 */
export function isPurl(purl: string): boolean {
	return true;
}

/**
 * Check if a string is a valid zurl
 * @todo Pretty obvious
 * @param {string} zurl The string to verify
 * @returns {boolean} true if the string is a zurl, false otherwise
 */
export function isZurl(zurl: string): boolean {
	return true;
}

/**
 * Check if the given string is a date
 * @param {string} s The string to check
 * @returns {boolean} true if the string is a date, false otherwise
 */
export function isDate(s: string): boolean {
	return !isNaN(Date.parse(s));
}

/**
 * Check if a given string is a date or null
 * @param {string} s The string to check
 * @returns {boolean} true if the string is a date or null, false otherwise
 */
export function isDateOrNull(s: string): boolean {
	return s === null ? true : isDate(s);
}

/**
 * Convert a string to a date or null if the string is null already
 * @param {string} s The string to convert
 * @returns {Date} the string converted to a date, or null if the string is already a null
 */
export function toDateOrNull(s: string): Date {
	return s === null ? null : validator.toDate(s);
}

/**
 * Check if a given value is a boolean or null
 * @param {any} obj The value to check
 * @returns {boolean} true if the value is a boolean or null
 */
export function isBooleanOrNull(obj: any): boolean {
	if(_.isString(obj))
		return validator.isBoolean(obj);

	return obj === null;
}

/**
 * Convert a given value to boolean or null
 * @param {any} obj The value to convert
 * @returns {boolean} the value converted to a boolean, or null
 */
export function toBooleanOrNull(obj: any): boolean {
	if(_.isString(obj))
		return validator.toBoolean(obj);
	
	if(_.isBoolean(obj))
		return obj;

	return null;
}