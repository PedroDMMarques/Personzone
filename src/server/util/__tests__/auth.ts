import { generatePasswordSalt, generateEmailVerificationHash, hashPassword } from "../auth";

describe("generatedPasswordSalt", () => {
	const salt = generatePasswordSalt();

	test("is string", () => {
		expect(typeof salt).toBe("string");
	});

	test("is 32 characters", () => {
		expect(salt.length).toBe(32);
	});
});

describe("generateEmailVerificationHash", () => {
	const hash = generateEmailVerificationHash();

	test("is string", () => {
		expect(typeof hash).toBe("string");
	});

	test("is 32 characters", () => {
		expect(hash.length).toBe(32);
	});
});

describe("hashPassword", () => {
	const salt = generatePasswordSalt();
	const password = "password";

	const hash = hashPassword(password, salt);

	test("is string", () => {
		expect(typeof hash).toBe("string");
	});

	test("is 64 characters", () => {
		expect(hash.length).toBe(64);
	});
});