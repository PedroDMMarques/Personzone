import _ from "lodash";
import { config as dotenv, DotenvResult } from "dotenv";

/**
 * Loads environment variables from a .env file
 * @param {string[]} [reqVars=[]] A list of required variables that should be present in the .env file
 * @throws {ReferenceError} if any of the variables specified in reqVars do not appear in the .env file
 */
export function loadEnvironment(reqVars: string[] = []) : void {
	const v: DotenvResult = dotenv();
	if(v.error)
		throw v.error;

	_.each(reqVars, function(s: string){
		if(_.isNil(v.parsed[s]))
			throw new ReferenceError(`Required variable '${s}' is not present in .env file`);
	});
}

/**
 * Detect whether a given object has a cyclic structure or not (some part of the object points to itself)
 * @todo Redo this function properly. Placeholder for now because I can't be bothered to think about a better way
 * @param {object} obj The object to check for a cyclic structure in
 * @returns {boolean} True if the object supplied has a cyclic structure, false otherwise 
 */
export function isCyclic(obj: object): boolean {
	try{
		JSON.stringify(obj);
		return false;
	}catch(err){
		return true;
	}
}

/**
 * Removes undefined keys from a given object
 * @todo Redo this function properly. Placeholder for now because I can't be bothered to think about a better way
 * @param {object} obj The object to remove undefined keys from
 * @returns {object} a copy of the object supplied without the undefined keys
 */
export function removeUndefined(obj: object): object {
	return JSON.parse(JSON.stringify(obj));
}

/**
 * Get a clone of an instance
 * @param {T} instance The instance to clone
 * @returns {T} a clone of the provided instance
 */
export function cloneInstance<T>(instance: T): T {
	return Object.assign(Object.create(Object.getPrototypeOf(instance)), instance);
}

/**
 * Check if the object is defined. The opposite of _.isUndefined
 * This is just for convenience and easier reading
 * @param {any} obj The object to check
 * @returns {boolean} true if it's defined, false otherwise
 */
export function isDefined(obj: any): boolean {
	return !_.isUndefined(obj);
}