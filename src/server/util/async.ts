import async from "async";
import winston from "winston";

interface ProcessBatchPrintFn {
	(batch: number, nBatches: number): void
}

export interface ProcessBatchOptions {
	printBatchNumber?: boolean
	printFn?: ProcessBatchPrintFn
}

export interface ProcessBatchFn {
	(batch: number): Promise<any>
}

/**
 * Process an async function for multiple batches in sequence
 * @param {number} nBatches The number of batches to perform the function for
 * @param {ProcessBatchFn} fn The function to call each batch
 * @param {ProcessBatchOptions} [options={}] Additional options
 * @returns {Promise<any>} a promise that resolves when all batches have been completed
 */
export function processBatches(nBatches: number, fn: ProcessBatchFn, options: ProcessBatchOptions={}): Promise<any> {
	const defaultPrintFn: ProcessBatchPrintFn = function(batch){
		winston.info(`Processing batch [${batch}/${nBatches}]`);
	}
	
	return new Promise(function(resolve, reject){
		let batch: number = 0;
		return async.whilst(
			function(){ return batch < nBatches },
			async function(callback){
				batch++;
				const f = options.printFn || defaultPrintFn;
				if(options.printBatchNumber)
					f(batch, nBatches);

				try{
					await fn(batch-1);
					callback(null);
				}catch(err){
					callback(err);
				}
			},
			function(err){
				return err ? reject(err) : resolve();
			}
		);
	});
}

/**
 * Get a promise that resolves after a determined number of milliseconds
 * @param {number} ms The number of milliseconds to sleep for
 * @returns {Promise<void>} promise that resolves after 'ms' milliseconds
 */
export function sleep(ms: number): Promise<void> {
	return new Promise((resolve, reject) => {
		setTimeout(resolve, ms);
	});
}