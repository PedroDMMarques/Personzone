import { Connection } from "typeorm";
import { Auth } from "../../models/Auth";
import { GetUserParams, getUser } from "./user";
import { User } from "../../models/User";
import { isDefined } from "../node";

export interface UpdateLastLoginParams {
	/**
	 * The user to update the auth object of
	 */
	user?: User
	/**
	 * The parameters to get the user if one wasn't supplied in "user"
	 */
	getUser?: GetUserParams
	/**
	 * The date to set the last login to. If not set will update the last login to NOW()
	 */
	date?: Date
}

/**
 * Update a user's last login parameter.
 * @param {Connection} db
 * @param {UpdateLastLoginParams} params
 * @throws {ReferenceError} If params are not supplied with either 'user' or 'getUser'
 * @returns {Promise<User>} a promise that resolves to the update user, with the auth relation
 */
export async function updateLastLogin(db: Connection, params: UpdateLastLoginParams): Promise<User> {
	let user: User;
	try{
		user = params.user || await getUser(db, params.getUser);
	}catch(e){
		throw new ReferenceError("Not supplied with 'user' or 'getUser'");
	}

	(<Auth> user.auth).lastLogin = params.date || new Date();
	await db.getRepository(User).save(user);
	return getUser(db, {id: user.id});
}

export interface ChangePasswordParams {
	/**
	 * The user to change the password of. Should contain the the auth relation 
	 */
	user?: User
	/**
	 * The parameters to get the user of. "user" parameter takes precedence
	 */
	getUser?: GetUserParams
	/**
	 * The new password
	 */
	newPass: string
	/**
	 * If set, will only update if the existing password is this one.
	 * If it fails throws a ReferenceError
	 */
	oldPass?: string
}

/**
 * Change a user's password. Returns a promise that resolves to the user, with the auth relation
 * @param {Connection} db
 * @param {ChangePasswordParams} params
 * @throws {ReferenceError} if oldPass is defined and does not correspond to the current password
 * @throws {ReferenceError} if params is not supplied with either 'user' or 'getUser'
 * @returns {Promise<User>} a promise that resolves to the user, with the auth relation
 */
export async function changePassword(db: Connection, params: ChangePasswordParams): Promise<User> {
	let user: User;
	try{
		user = params.user || await getUser(db, params.getUser);
	}catch(e){
		throw new ReferenceError("Not supplied with 'user' or 'getUser'");
	}

	const auth = <Auth> user.auth;
	if(isDefined(params.oldPass)){
		if(!auth.validatePassword(params.oldPass))
			throw new ReferenceError("oldPass is incorrect");
	}

	auth.password = params.newPass;
	await db.getRepository(User).save(user);
	return getUser(db, {id: user.id});
}