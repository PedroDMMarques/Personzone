import _ from "lodash";

import { Connection } from "typeorm";
import { UserEmail } from "../../models/UserEmail";
import { isDefined } from "../node";
import { User } from "../../models/User";
import { GetUserParams, getUser } from "./user";

export interface GetEmailParams {
	/**
	 * The id of the email to get. This param takes precedence over 'email'
	 */
	id?: number
	/**
	 * The "name" of the email to get. 'Id' param takes precedence over this
	 */
	email?: string
}

/**
 * Get a user email, with the user relation
 * @param {Connection} db The database connection to use
 * @param {GetEmailParams} params
 * @returns {Promise<UserEmail>} a promise that resolves to the user email, with the user relation
 */
export async function getEmail(db: Connection, params: GetEmailParams): Promise<UserEmail> {
	let whereClause: string = "";
	let whereParams: {[s:string]: any} = {};

	if(isDefined(params.id)){
		whereClause += "email.id = :id";
		whereParams.id = params.id;
	}else if(isDefined(params.email)){
		whereClause += "email.email = :email";
		whereParams.email = params.email;
	}else{
		throw new ReferenceError("'id' or 'email' must be specified in the parameters");
	}

	return await db.getRepository(UserEmail).createQueryBuilder("email")
		.leftJoinAndSelect("email.user", "user")
		.where(whereClause, whereParams)
		.getOne();
}

export interface GetUserPrimaryEmailParams {
	/**
	 * A user object to get the primary email for. This parameter takes precedence over 'getUser'
	 */
	user?: User

	/**
	 * If a user object is not supplied then get a user based on these parameters
	 */
	getUser?: GetUserParams
}

/**
 * Get a user's primary email
 * @param {Connection} db The database connection to use
 * @param {GetUserPrimaryEmailParameters} params
 * @returns {Promise<UserEmail>} a promise that resolves to the user's primary email
 */
export async function getUserPrimaryEmail(db: Connection, params: GetUserPrimaryEmailParams): Promise<UserEmail> {
	let user = params.user || await getUser(db, params.getUser);
	let e: UserEmail;
	
	_.each(user.emails, function(email: UserEmail){
		if(email.primary)
			e = email;

		// Returns false and stops the loop if we already found the primary email
		return !isDefined(e);

	});

	return getEmail(db, { id: e.id });
}

export interface VerifyEmailParams {
	/**
	 * The email object to verify. This parameter takes precedence over "getEmail"
	 */
	email?: UserEmail
	/**
	 * If an email object is not provided, search for one based on this parameters
	 */
	getEmail?: GetEmailParams
	/**
	 * If provided will only verify the email if the email's verification hash matches this parameter
	 * If this parameter is not provided, the email will be verified without further checks
	 */
	hash?: string
}

/**
 * Verify an email, even if the email was already verified before (does nothing in that case, but still returns true)
 * @param {Connection} db
 * @param {VerifyEmailParams} params
 * @returns {Promise<boolean>} a promise that resolves to true if the verification of successful, or false otherwise
 */
export async function verifyEmail(db: Connection, params: VerifyEmailParams): Promise<boolean> {
	const email = params.email || await getEmail(db, params.getEmail);

	if(email.verified)
		return true;

	if(isDefined(params.hash))
		email.verified = params.hash === email.verificationHash;
	else
		email.verified = true;
	
	if(email.verified){
		await db.getRepository(UserEmail).save(email);
		return true;
	}else{
		return false;
	}
}

export interface SetEmailPrimaryParams {
	/**
	 * The email object to set primary. This parameter takes precedence over "getEmail"
	 */
	email?: UserEmail
	/**
	 * If an email object is not provided then use this parameter to search for the email
	 */
	getEmail?: GetEmailParams
}

/**
 * Set an email as primary, and remove the primary value of the currently primary email
 * If the email to set as primary is already primary then does nothing (and still returns true)
 * @param {Connection} db
 * @param {SetEmailPrimaryParams} params
 * @returns {Promise<boolean>} a promise that resolves to true when finished
 */
export async function setEmailPrimary(db: Connection, params: SetEmailPrimaryParams): Promise<boolean> {
	const email = params.email || await getEmail(db, params.getEmail);
	if(email.primary)
		return true;

	const primaryEmail = await getUserPrimaryEmail(db, {getUser: {id: email.user.id}});
	primaryEmail.primary = false;
	email.primary = true;

	await db.getRepository(UserEmail).save(email);
	await db.getRepository(UserEmail).save(primaryEmail);
	return true;
}

export interface DeleteEmailParams {
	/**
	 * The email object to delete. This parameter takes precedence over "getEmail"
	 */
	email?: UserEmail
	/**
	 * If an email object was not given then use this parameter to search for the email to delete
	 */
	getEmail?: GetEmailParams
	/**
	 * By default will not delete an email set as primary. If this is set to true then it will force the deletion on primary emails
	 */
	force?: boolean
}

/**
 * Delete an email. By default will not delete emails set as primary. Use params.force to force the deletion on primary emails
 * @param {Connection} db
 * @param {DeleteEmailParams} params
 * @returns {Promise<boolean>} a promise that resolves to true if the email was successfully deleted, false otherwise
 */
export async function deleteEmail(db: Connection, params: DeleteEmailParams): Promise<boolean> {
	const email = params.email || await getEmail(db, params.getEmail);
	if(email.primary && !params.force)
		return false;

	await db.getRepository(UserEmail).remove(email);
	return true;
}

export interface CreateEmailParams {
	/**
	 * The user to create the email for. This takes precedence over "getUser"
	 */
	user?: User
	/**
	 * If a user object was not provided use this parameter to search for the user
	 */
	getUser?: GetUserParams
	/**
	 * The "name" for the email
	 */
	email: string
}

/**
 * Create an email
 * @param {Connection} db
 * @param {CreateEmailParams} params
 * @returns {Promise<UserEmail>} a promise that resolves to the created email
 */
export async function createEmail(db: Connection, params: CreateEmailParams): Promise<UserEmail> {
	const user = params.user || await getUser(db, params.getUser);
	const email = new UserEmail();
	email.email = params.email;
	email.user = user;

	return await db.getRepository(UserEmail).save(email);
}