import _ from "lodash";
import crypto from "crypto";

import { loadEnvironment } from "../../node";
import { Connection, AdvancedConsoleLogger } from "typeorm";
import { createDatabase } from "../../../database";
import { createUser } from "../user";
import { User } from "../../../models/User";
import { Zone } from "../../../models/Zone";
import { ZONE_TYPES } from "../../enums";
import { POSTGRES_ERROR_CODES } from "../../postgresErrorCodes";
import { Auth } from "../../../models/Auth";

import { isCyclic } from "../../node";
import { UserEmail } from "../../../models/UserEmail";

loadEnvironment([
	"DATABASE_HOST",
	"DATABASE_PORT",
	"DATABASE_USER",
	"DATABASE_PASS",
	"DATABASE_NAME",
]);

let db: Connection;

// Connect to the database
beforeAll(async () => {
	db = await createDatabase({
		host: process.env.DATABASE_HOST,
		port: _.toNumber(process.env.DATABASE_PORT),
		user: process.env.DATABASE_USER,
		pass: process.env.DATABASE_PASS,
		database: process.env.DATABASE_NAME,
		entitiesAsTs: true,
		printConnectionDetails: false,
	});
});

//@todo
describe("updateUser", () => {
	test("should work with number as identifier");
	test("should work with string as identifier");
	test("should update all fields");
	test("should return non-cyclic user structure");
});

describe("createUser basic", () => {
	const name = "Pedro Marques";
	const email = `${crypto.randomBytes(16).toString("hex")}@yopmail.com`;
	const password = "pass";

	let createdUser: User = null;

	// Create the user
	beforeAll(async () => {
		createdUser = await createUser(db, {name, email, password});
	});

	// Delete the user and close the database connection
	afterAll(async () => {
		await db.getRepository(User).remove(createdUser);
		await db.close();
	});

	test("returned user should not have a cyclic structure", () => {
		expect(isCyclic(createdUser)).toBe(false);
	});

	test("user and relations should exist in database should exist in database", async () => {
		const user = await db.getRepository(User)
			.findOne({
				where: { purl: createdUser.purl },
				relations: ["auth", "privacy", "zones", "emails"],
			});

		expect(user).toBeDefined();
		expect(user.auth).toBeDefined();
		expect(user.privacy).toBeDefined();
		expect(user.zones.length).toBeGreaterThan(0);
		expect(user.emails.length).toBeGreaterThan(0);
	});

	test("should fail creating a user with a duplicate email", async done => {
		try{
			await createUser(db, {name, email, password});
			done.fail(new Error("Create user with duplicate email did not throw error"));
		}catch(err){
			expect(err.code).toBe(POSTGRES_ERROR_CODES.UNIQUE_CONSTRAINT);
			done();
		}
	});

	test("created user with correct values", async () => {
		const user = await db.getRepository(User)
			.findOne({
				where: { purl: createdUser.purl },
				relations: ["auth", "privacy", "zones", "emails"],
			});

		expect(user.name).toBe(name);
		expect((<UserEmail> user.emails[0]).email).toBe(email);
		expect((<UserEmail> user.emails[0]).primary).toBe(true);
		expect((<UserEmail> user.emails[0]).verified).toBe(false);
		expect((<Zone> user.zones[0]).type).toBe(ZONE_TYPES.PERSONAL);
		expect((<Zone> user.zones[0]).name).toBe("Personal");
		expect((<Auth> user.auth).validatePassword(password)).toBe(true);
	});
});