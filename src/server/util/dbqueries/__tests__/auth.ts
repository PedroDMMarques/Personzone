import _ from "lodash";
import crypto from "crypto";

import { loadEnvironment, isCyclic } from "../../node";
import { Connection, AdvancedConsoleLogger } from "typeorm";
import { createDatabase } from "../../../database";
import { createUser, getUser } from "../user";
import { updateLastLogin, changePassword } from "../auth";
import { User } from "../../../models/User";
import { Auth } from "../../../models/Auth";
import { sleep } from "../../async";

loadEnvironment([
	"DATABASE_HOST",
	"DATABASE_PORT",
	"DATABASE_USER",
	"DATABASE_PASS",
	"DATABASE_NAME",
]);

let db: Connection;

// Connect to the database
beforeAll(async () => {
	db = await createDatabase({
		host: process.env.DATABASE_HOST,
		port: _.toNumber(process.env.DATABASE_PORT),
		user: process.env.DATABASE_USER,
		pass: process.env.DATABASE_PASS,
		database: process.env.DATABASE_NAME,
		entitiesAsTs: true,
		printConnectionDetails: false,
	});
});

describe("updateLastLogin", () => {
	const name = "Pedro Marques";
	const email = `${crypto.randomBytes(16).toString("hex")}@yopmail.com`;
	const password = "pass";

	let createdUser: User = null;

	// Create user
	beforeEach(async () => {
		createdUser = await createUser(db, {name, email, password});
	});

	// Delete the user
	afterEach(async () => {
		await db.getRepository(User).remove(createdUser);
	});

	test("should update to specific date if called with 'user' object", async () => {
		let user = await getUser(db, {id: createdUser.id});
		let date = new Date(2000, 1, 1);
		user = await updateLastLogin(db, {user, date});
		expect((<Auth> user.auth).lastLogin.getTime()).toBe(date.getTime());
	});

	test("should update to specific date if called with 'getUser' object", async () => {
		let user = await getUser(db, {id: createdUser.id});
		let date = new Date(2000, 1, 1);
		user = await updateLastLogin(db, {getUser: {id: user.id}, date});
		expect((<Auth> user.auth).lastLogin.getTime()).toBe(date.getTime());
	});

	test("should throw ReferenceError if called with 'user' or 'getUser'", async (done) => {
		try{
			await updateLastLogin(db, {});
			done(new Error("Did not throw error without 'user' or 'getUser'"));
		}catch(e){
			done(expect(e).toBeInstanceOf(ReferenceError));
		}
	});

	test("should update to NOW() correctly", async () => {
		let user = await getUser(db, {id: createdUser.id});
		let prevLogin = (<Auth> user.auth).lastLogin;
		await sleep(2000);
		user = await updateLastLogin(db, {user});
		expect((<Auth> user.auth).lastLogin.getTime()).toBeGreaterThanOrEqual(prevLogin.getTime() + 2000);
	});

	test("should not return a cyclic structure", async () => {
		let user = await getUser(db, {id: createdUser.id});
		user = await updateLastLogin(db, {user});
		expect(isCyclic(user)).toBe(false);
	});

});

describe("changePassword", () => {
	const name = "Pedro Marques";
	const email = `${crypto.randomBytes(16).toString("hex")}@yopmail.com`;
	const password = "pass";

	let createdUser: User = null;

	// Create user
	beforeEach(async () => {
		createdUser = await createUser(db, {name, email, password});
	});

	// Delete the user
	afterEach(async () => {
		await db.getRepository(User).remove(createdUser);
	});

	test("should change if supplied with 'user'", async () => {
		let user = await getUser(db, {id: createdUser.id});
		user = await changePassword(db, {user, newPass: "newPass"});
		expect((<Auth> user.auth).validatePassword("newPass")).toBe(true);
	});

	test("should change if supplied with 'getUser'", async () => {
		let user = await getUser(db, {id: createdUser.id});
		user = await changePassword(db, {getUser: {id: user.id}, newPass: "newPass"});
		expect((<Auth> user.auth).validatePassword("newPass")).toBe(true);
	});

	test("should change if supplied with correct last password", async () => {
		let user = await getUser(db, {id: createdUser.id});
		user = await changePassword(db, {user, newPass: "newPass", oldPass: password});
		expect((<Auth> user.auth).validatePassword("newPass")).toBe(true);
	});

	test("should throw ReferenceError if supplied with incorrect old password", async (done) => {
		try{
			let user = await getUser(db, {id: createdUser.id});
			user = await changePassword(db, {user, newPass: "newPass", oldPass: "somethingelse"});
			done(new Error("should have throw ReferenceError with incorrect last password"));
		}catch(e){
			done(expect(e).toBeInstanceOf(ReferenceError));
		}
	});

	test("should not return a cyclic structure", async () => {
		let user = await getUser(db, {id: createdUser.id});
		user = await changePassword(db, {user, newPass: "newPass"});
		expect(isCyclic(user)).toBe(false);
	});
});