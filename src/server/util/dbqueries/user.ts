import _ from "lodash";

import { Connection } from "typeorm";
import { User } from "../../models/User";
import { generatePurl, generateZurl } from "../models";
import { Auth } from "../../models/Auth";
import { Zone } from "../../models/Zone";
import { ZONE_TYPES, PRIVACY_SETTINGS, GENDERS, BLOOD_TYPES, REL_STATUSES } from "../enums";
import { UserPrivacy } from "../../models/UserPrivacy";
import { isDefined } from "../node";
import { UserEmail } from "../../models/UserEmail";

export interface CreateUserParams {
	/**
	 * The name of the user
	 */
	name: string

	/**
	 * The email of the user
	 */
	email: string

	/**
	 * The password of the user
	 */
	password: string
}

/**
 * Create a user and save it to the database
 * @todo Optimization
 * @param {Connection} db The database connection to use
 * @param {CreateUserParams} args The arguments to use for the user
 * @returns {Promise<User>} a promise that resolves when the user has been created in the database
 */
export async function createUser(db: Connection, params: CreateUserParams): Promise<User> {
	const auth = new Auth();
	auth.password = params.password;
	
	const zone = new Zone();
	zone.name = "Personal";
	zone.type = ZONE_TYPES.PERSONAL;
	zone.zurl = generateZurl(zone.name);
	
	const email = new UserEmail();
	email.email = params.email;
	email.primary = true;

	const privacy = new UserPrivacy();
	
	const user = new User();
	user.name = params.name;
	user.purl = generatePurl(user.name);
	
	zone.user = user;
	user.privacy = privacy;
	user.zones = [zone];
	user.emails = [email];
	user.auth = auth;

	await db.getRepository(User).save(user);
	return db.getRepository(User).findOne({
		where: { purl: user.purl },
		relations: ["auth", "zones", "privacy", "emails"],
	});
}

export interface UpdateUserParams {
	user?: User,
	getUser?: GetUserParams,
	
	name?: string
	purl?: string
	gender?: GENDERS
	dob?: Date
	bloodType?: BLOOD_TYPES
	drivingLicense?: boolean
	seekingWork?: boolean
	relStatus?: REL_STATUSES
	defWord?: string

	joinDatePrivacy?: PRIVACY_SETTINGS
	genderPrivacy?: PRIVACY_SETTINGS
	dobPrivacy?: PRIVACY_SETTINGS
	bloodTypePrivacy?: PRIVACY_SETTINGS
	drivingLicensePrivacy?: PRIVACY_SETTINGS
	seekingWorkPrivacy?: PRIVACY_SETTINGS
	relStatusPrivacy?: PRIVACY_SETTINGS
	defWordPrivacy?: PRIVACY_SETTINGS
}

/**
 * Update a user
 * @param {Connection} db
 * @param {UpdateUserParams} params
 * @returns {Promise<User>} a promise that resolves to the updated user
 */
export async function updateUser(db: Connection, params: UpdateUserParams): Promise<User> {
	const user = params.user || await getUser(db, params.getUser);

	if(isDefined(params.name))
		user.name = params.name;
	
	if(isDefined(params.purl))
		user.purl = params.purl;

	if(params.gender !== undefined)
		user.gender = params.gender;

	if(params.dob !== undefined)
		user.dob = params.dob;

	if(params.bloodType !== undefined)
		user.bloodType = params.bloodType;

	if(params.drivingLicense !== undefined)
		user.drivingLicense = params.drivingLicense;

	if(params.seekingWork !== undefined)
		user.seekingWork = params.seekingWork;

	if(params.relStatus !== undefined)
		user.relStatus = params.relStatus;

	if(params.defWord !== undefined)
		user.defWord = params.defWord;

	const privacy = <UserPrivacy> user.privacy;
	if(isDefined(params.joinDatePrivacy))
		privacy.joinDate = params.joinDatePrivacy;

	if(isDefined(params.genderPrivacy))
		privacy.gender = params.genderPrivacy;

	if(isDefined(params.dobPrivacy))
		privacy.dob = params.dobPrivacy;

	if(isDefined(params.bloodTypePrivacy))
		privacy.bloodType = params.bloodTypePrivacy;

	if(isDefined(params.drivingLicensePrivacy))
		privacy.drivingLicense = params.drivingLicensePrivacy;

	if(isDefined(params.seekingWorkPrivacy))
		privacy.seekingWork = params.seekingWorkPrivacy;

	if(isDefined(params.relStatusPrivacy))
		privacy.relStatus = params.relStatusPrivacy;

	if(isDefined(params.defWordPrivacy))
		privacy.defWord = params.defWordPrivacy;

	await db.getRepository(User).save(user);
	return getUser(db, {id: user.id});
}

export interface GetUserParams {
	/**
	 * The id of the user to get. This parameter takes priority over any other search parameter
	 */
	id?: number,
	/**
	 * The purl of the user to get. This parameter takes priority after the id
	 */
	purl?: string,
	/**
	 * The email of the user to get. This parameter takes priority after the purl, and only works for emails tagged as "primary"
	 */
	email?: string,
}

/**
 * Get an existing user
 * @param {Connection} db The database connection to use
 * @param {GetUserParams} params The arguments to use for the search
 * @returns {Promise<User>} a promise that resolves to the created user
 */
export async function getUser(db: Connection, params: GetUserParams): Promise<User> {
	let whereClause: string = "";
	let whereParams: {[name:string]: any} = {};

	if(!_.isUndefined(params.id)){
		whereClause = "user.id = :id";
		whereParams.id = params.id;
	}else if(!_.isUndefined(params.purl)){
		whereClause = "user.purl = :purl";
		whereParams.purl = params.purl;
	}else if(!_.isUndefined(params.email)){
		whereClause = "email.email = :email AND email.primary = :primary";
		whereParams.email = params.email;
		whereParams.primary = true;
	}
	
	return db.getRepository(User).createQueryBuilder("user")
		.leftJoinAndSelect("user.emails", "email")
		.leftJoinAndSelect("user.zones", "zone")
		.leftJoinAndSelect("user.auth", "auth")
		.leftJoinAndSelect("user.privacy", "privacy")
		.where(whereClause, whereParams)
		.getOne();
}