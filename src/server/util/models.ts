import uuidv1 from "uuid/v1";
import crypto from "crypto";

/**
 * Generate a purl based on a name
 * @param {string} name The name to generate the purl for
 * @returns {string} the generated purl
 */
export function generatePurl(name: string): string {
	const nameSplit = name.split(" ");
	const hash = crypto.createHash("md5").update(uuidv1()).digest("hex").substring(0, 12);
	return `${nameSplit[0]}.${nameSplit[nameSplit.length-1]}.${hash}`;
}

/**
 * Generate a zurl based on a name
 * @param {string} name The name to generate the zurl for
 * @returns {string} the generated zurl
 */
export function generateZurl(name: string): string {
	const hash = crypto.createHash("md5").update(uuidv1()).digest("hex").substring(0, 12);
	return `${name.split(" ").join("-")}-${hash}`;
}

/**
 * Transform a given string into "Title Case", where every word has only it's first letter capitalized
 * Used for user names among others
 * @param {string} str The string to convert to "Title Case"
 * @returns {string}
 */
export function toTitleCase(str: string): string {
    return str.replace(/\w\S*/g, function(txt){
        return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
    });
}