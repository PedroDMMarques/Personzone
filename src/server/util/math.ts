import _ from "lodash";

export interface Weight<T> {
	value: T
	probability: number
};

/**
 * Select a random value from a list of weights
 * @param {Weight<T>[]} weights An array of Weights to select from
 * @returns {T | void} a randomly choosen value from the array of supplied weights
 */
export function weightedRNG<T>(weights: Weight<T>[]): T{
	let max = 0;
	let weightIndex: number[] = [];
	_.each(weights, weight => {
		max += weight.probability
		weightIndex.push(max);
	});

	let rng = _.random(1, max);
	let index = _.findIndex(weightIndex, n => n >= rng);
	return weights[index].value;
}

/**
 * Generates a random date between two other dates
 * @param {Date} min The minimum date to generate a new one between
 * @param {Date} max The maximum date to generate a new one between
 * @returns {Date} the generated date
 */
export function randomDate(min: Date, max: Date): Date {
	return new Date(min.getTime() + Math.random() * (max.getTime() - min.getTime()));
}