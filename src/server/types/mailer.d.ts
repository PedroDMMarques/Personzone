import { Mailer } from "../middleware/mailer/Mailer";

declare global {
	namespace Express {
		interface Request {
			/**
			 * The mailer object
			 */
			mailer: Mailer
		}
	}
}