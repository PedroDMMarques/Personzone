import { NODE_ENV } from "../util/enums";

declare global {

	namespace NodeJS {
		interface ProcessEnv {
			/**
			 * Current node environment
			 */
			NODE_ENV: NODE_ENV
			
			/**
			 * The port the server should listen on (should be case to number before using)
			 */
			SERVER_PORT: string
			
			/**
			 * The secret to initialize the server session with
			 */
			SERVER_SESSION_SECRET: string

			/**
			 * Database host name
			 */
			DATABASE_HOST: string
			
			/**
			 * Database port number (should be cast to number before using)
			 */
			DATABASE_PORT: string
			
			/**
			 * Database user name
			 */
			DATABASE_USER: string
			
			/**
			 * Database user password
			 */
			DATABASE_PASS: string
			
			/**
			 * Database name
			 */
			DATABASE_NAME: string

			/**
			 * The SMTP server host
			 */
			MAILER_HOST: string

			/**
			 * The port to connect to on the SMTP server
			 */
			MAILER_PORT: string

			/**
			 * The user on the SMTP server
			 */
			MAILER_USER: string

			/**
			 * The password for the SMTP server
			 */
			MAILER_PASS: string
		}
	}
} 