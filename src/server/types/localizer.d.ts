import { Connection } from "typeorm";
import { Localizer } from "../middleware/localizer/Localizer";

declare global {
	namespace Express {
		interface Request {
			localizer: Localizer
		}
	}
}