import { Connection } from "typeorm";

declare global {
	namespace Express {
		interface Request {
			/**
			 * Database connection object
			 */
			db: Connection
		}
	}
}