import { ApiError } from "./ApiError";

export class UnknownError extends ApiError {
	constructor(message?: string, origin?: any){
		super(`UnknownError: ${message}`, origin);
	}

	getResponseStatus(){
		return 500;
	}

	getResponseObject(){
		return {
			error: "UnknownError",
			message: "An unknown error has occurred",
		}
	}
}