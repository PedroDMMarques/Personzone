import { ApiError } from "./ApiError";

export class EmailIsPrimaryError extends ApiError {
	constructor(public email?: string, public id?: number, origin?: any){
		super("EmailIsPrimaryError", origin);
	}

	getResponseStatus(){
		return 400;
	}

	getResponseObject(){
		const email = this.email && `[${this.email}]`;
		const id = this.id && `[id=${this.id}]`;
		return {
			error: "EmailIsPrimaryError",
			message: `Email '${email || ""}${id || ""}' is set as primary`,
		}
	}
}