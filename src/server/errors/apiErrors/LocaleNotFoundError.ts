import { ApiError } from "./ApiError";

export class LocaleNotFoundError extends ApiError {
	constructor(public locale: string, origin?: any){
		super("LocaleNotFoundError", origin);
	}

	getResponseStatus(){
		return 404;
	}

	getResponseObject(){
		return {
			error: "LocaleNotFoundError",
			message: `Locale '${this.locale}' could not be found`,
		}
	}
}