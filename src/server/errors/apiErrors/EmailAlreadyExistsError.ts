import { ApiError } from "./ApiError";

export class EmailAlreadyExistsError extends ApiError {
	constructor(public email: string, origin?: any){
		super("EmailAlreadyExistsError", origin);
	}

	getResponseStatus(){
		return 409;
	}

	getResponseObject(){
		return {
			error: "EmailAlreadyExistsError",
			message: `Email '${this.email}' already exists`,
		}
	}
}