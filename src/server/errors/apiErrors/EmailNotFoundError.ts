import { ApiError } from "./ApiError";

export class EmailNotFoundError extends ApiError {
	constructor(public email?: string, public id?: number, origin?: any){
		super("EmailNotFoundError", origin);
	}

	getResponseStatus(){
		return 404;
	}

	getResponseObject(){
		const email = this.email && `[${this.email}]`;
		const id = this.id && `[id=${this.id}]`;
		return {
			error: "EmailNotFoundError",
			message: `Email '${email || ""}${id || ""}' was not found`,
		}
	}
}