import { ApiError } from "./ApiError";

export class UserNotFoundError extends ApiError {
	constructor(public purl?: string, public id?: number, origin?: any){
		super("UserNotFoundError", origin);
	}

	getResponseStatus(){
		return 404;
	}

	getResponseObject(){
		const purl = this.purl && `[${this.purl}]`;
		const id = this.id && `[id=${this.id}]`;
		return {
			error: "UserNotFoundError",
			message: `User '${purl || ""}${id || ""}' was not found`,
		}
	}
}