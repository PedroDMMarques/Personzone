import { ApiError } from "./ApiError";

export class InvalidEmailVerificationHashError extends ApiError {
	constructor(public hash?: string, origin?: any){
		super("InvalidEmailVerificationHashError", origin);
	}

	getResponseStatus(){
		return 409;
	}

	getResponseObject(){
		return {
			error: "InvalidEmailVerificationHashError",
			message: `Verification hash '${this.hash}' is not valid`,
		}
	}
}