interface ResponseObject {
	error: string
	message: string
}

interface DevResponseObject extends ResponseObject {
	dev: {
		name: string
		message: string
		stack: string
	}
}

export abstract class ApiError extends Error {
	constructor(message?: string, public origin?: any){
		super(message);
	}

	/**
	 * Get the status code that should be sent when this error occurs
	 * @returns {number}
	 */
	abstract getResponseStatus(): number

	/**
	 * Get a response object that should be sent when this error occurs in production mode
	 * @returns {ResponseObject}
	 */
	abstract getResponseObject(): ResponseObject

	/**
	 * Get a response object that should be sent when this error occurs in development mode
	 * @returns {DevResponseObject}
	 */
	getDevResponseObject(): DevResponseObject {
		return Object.assign({}, this.getResponseObject(), {
			dev: {
				origin: this.origin && {
					name: this.origin.name,
					message: this.origin.message,
					stack: this.origin.stack
				},
				name: this.name,
				message: this.message,
				stack: this.stack,
			}
		});
	}

}