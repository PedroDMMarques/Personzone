import { ApiError } from "./ApiError";
import { Result } from "express-validator/check";

export class RequestValidationError extends ApiError {
	constructor(public validationError: Result, origin?: any){
		super("RequestValidationError", origin);
	}

	getResponseStatus(){
		return 400;
	}

	getResponseObject(){
		return {
			error: "RequestValidationError",
			message: "There was an error validating the request",
			validations: this.validationError.mapped(),
		}
	}
}