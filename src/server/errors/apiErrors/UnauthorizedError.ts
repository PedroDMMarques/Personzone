import { ApiError } from "./ApiError";
import { Result } from "express-validator/check";

export class UnauthorizedError extends ApiError {
	constructor(origin?: any){
		super("UnauthorizedError", origin);
	}

	getResponseStatus(){
		return 401;
	}

	getResponseObject(){
		return {
			error: "UnauthorizedError",
			message: "You are not authorized to make that request",
		}
	}
}