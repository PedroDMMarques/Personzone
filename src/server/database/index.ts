import path from "path";
import _ from "lodash";

import winston from "winston";
import { createConnection, Connection } from "typeorm";

// Required import for typeorm
import "reflect-metadata";

export interface DatabaseOptions {
	/**
	 * Host (url or localhost) of the database
	 */
	host: string,

	/**
	 * Listening port of the database
	 */
	port: number,

	/**
	 * User to connect to in the database
	 */
	user: string,

	/**
	 * Database of the user
	 */
	pass: string,

	/**
	 * Name of the database to use
	 */
	database: string,

	/**
	 * If set to true will clean the database and recreate it (synchronize)
	 */
	sync?: boolean,

	/**
	 * If set to false will not print any connection details
	 */
	printConnectionDetails?: boolean,

	/**
	 * Sets typeorm database to read entity (model) files as .ts instead of .js
	 * This is used for jest tests being run in typescript (ts-jest)
	 */
	entitiesAsTs?: boolean,
}

/**
 * Create a database connection
 * @param {DatabaseOptions} the database options to use
 * @returns {Promise<Connection>} a promise that resolves to a Connection object once a connection with the database was successful
 */
export function createDatabase(options: DatabaseOptions): Promise<Connection>{
	if(_.isUndefined(options.printConnectionDetails))
		options.printConnectionDetails = true;

	if(options.printConnectionDetails)
		winston.info(`Connecting to database ${options.user}@${options.host}:${options.port} - Database: ${options.database}`);

	return createConnection({
		type: "postgres",
		host: options.host,
		port: options.port,
		username: options.user,
		password: options.pass,
		database: options.database,
		schema: "public",
		synchronize: options.sync,
		dropSchema: options.sync,
		entities: [
			// Read *.js after compilation or .ts files because we are testing or something else
			path.join(__dirname, "..", "models", (options.entitiesAsTs ? "*.ts" : "*.js"))
		],
		//logging: true,
	});
}