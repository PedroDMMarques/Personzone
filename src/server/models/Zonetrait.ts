import { Model } from "./Model";
import { Entity, Column, ManyToOne, JoinColumn, OneToOne, OneToMany, Index } from "typeorm";
import { Trait } from "./Trait";
import { Zone } from "./Zone";
import { Rating } from "./Rating";

@Entity("zonetraits")
@Index(["trait", "zone"], { unique: true })
export class Zonetrait extends Model {

	@Column({
		name: "rank_updated_at",
		default: () => "NOW()",
	})
	rankUpdatedAt?: Date;

	@Column({
		name: "rank_outdated",
		default: true,
	})
	rankOutdated?: boolean;

	@Column({
		name: "number_ratings",
		default: 0,
	})
	numberRatings?: number;

	@Column({
		name: "average_rating",
		default: 0,
	})
	averageRating?: number;

	@ManyToOne(type => Trait)
	@JoinColumn({
		name: "trait_id",
	})
	trait?: Trait | Model;

	@ManyToOne(type => Zone)
	@JoinColumn({
		name: "zone_id",
	})
	zone?: Zone | Model;

	@OneToMany(type => Rating, rating => rating.zonetrait)
	ratings?: Rating[] | Model[];

}