import { Model } from "./Model";
import { Entity, Column, ManyToOne, JoinColumn } from "typeorm";
import { generateEmailVerificationHash } from "../util/auth";
import { User } from "./User";
import { PRIVACY_SETTINGS } from "../util/enums";

@Entity("user_emails")
export class UserEmail extends Model {
	constructor(){
		super();
		this.verificationHash = generateEmailVerificationHash();
	}
	
	@Column({
		name: "email",
		unique: true,
	})
	email: string;

	@Column({
		name: "verified",
		default: false,
	})
	verified: boolean;

	@Column({
		name: "verification_hash",
	})
	verificationHash: string;

	@Column({
		name: "primary",
		default: false
	})
	primary: boolean;

	@Column({
		name: "privacy",
		default: PRIVACY_SETTINGS.PRIVATE,
	})
	privacy: PRIVACY_SETTINGS;

	@ManyToOne(type => User, {
		onDelete: "CASCADE",
	})
	@JoinColumn({
		name: "user_id",
	})
	user: Model | User;
}