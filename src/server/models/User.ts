import { Entity, PrimaryGeneratedColumn, Column, CreateDateColumn, UpdateDateColumn, OneToOne, ManyToMany, OneToMany, Connection, JoinColumn } from "typeorm";
import { GENDERS, BLOOD_TYPES, REL_STATUSES, PRIVACY_SETTINGS } from "../util/enums";
import { Auth } from "./Auth";
import { Location } from "./Location";
import { UserLocation } from "./UserLocation";
import { Model } from "./Model";
import { UserPrivacy } from "./UserPrivacy";
import { Zone } from "./Zone";
import { toTitleCase } from "../util/models";
import { cloneInstance } from "../util/node";
import { UserEmail } from "./UserEmail";

@Entity("users")
export class User extends Model {

	@Column({
		name: "name",
	})
	_name: string;
	
	set name(name){
		this._name = toTitleCase(name);
	}

	get name(){
		return this._name;
	}
	
	@Column({
		name: "purl",
		unique: true,
	})
	purl: string;

	@Column({
		name: "gender",
		nullable: true,
	})
	gender?: GENDERS;

	@Column({
		name: "score",
		default: 0,
	})
	score?: number;

	@Column({
		name: "score_updated_at",
		nullable: true,
	})
	scoreUpdateAt?: Date;

	@Column({
		name: "blood_type",
		nullable: true,
	})
	bloodType?: BLOOD_TYPES;

	@Column({
		name: "dob",
		nullable: true,
	})
	dob?: Date;

	@Column({
		name: "driving_license",
		nullable: true,
	})
	drivingLicense?: boolean;

	@Column({
		name: "seeking_work",
		nullable: true,
	})
	seekingWork?: boolean;

	@Column({
		name: "rel_status",
		nullable: true,
	})
	relStatus?: REL_STATUSES;

	@Column({
		name: "def_word",
		nullable: true,
	})
	defWord?: string;

	@OneToOne(type => Auth, auth => auth.user, {
		cascade: true,
	})
	auth?: Auth | Model;

	@OneToMany(type => UserEmail, userEmail => userEmail.user, {
		cascade: true,
		onDelete: "CASCADE",
	})
	emails?: UserEmail[] | Model[];

	@OneToMany(type => UserLocation, userLocation => userLocation.user, {
		cascade: true,
		onDelete: "CASCADE",
	})
	locations?: UserLocation[] | Model[];

	@OneToMany(type => Zone, zone => zone.user, {
		cascade: true,
		onDelete: "CASCADE",
	})
	zones?: Zone[] | Model[];

	@OneToOne(type => UserPrivacy, privacy => privacy.user, {
		cascade: true,
	})
	privacy?: UserPrivacy | Model;

	/**
	 * Get a public profile clone of a user (removing elements that are set above "PUBLIC")
	 * @static
	 * @param {User} user The user to clone
	 * @returns {User} a clone of the user instance provided, with elements only visible as PUBLIC 
	 */
	static getPublicProfile(user: User): User {
		user = cloneInstance(user);
		return user;
	}

	/**
	 * Get a friends profile clone of a user (removing elements that are set above "FRIENDS")
	 * @static
	 * @param {User} user The user to clone
	 * @returns {User} a clone of the user instance provided, with elements only visible as FRIENDS 
	 */
	static getFriendsProfile(user: User): User {
		return cloneInstance(user);
	}

	/**
	 * Get a private profile clone of a user (removing elements that are set above "PRIVATE")
	 * @static
	 * @param {User} user The user to clone
	 * @returns {User} a clone of the user instance provided, with elements only visible as PRIVATE 
	 */
	static getPrivateProfile(user: User): User {
		return cloneInstance(user);
	}
}