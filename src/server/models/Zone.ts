import { Entity, Column, PrimaryGeneratedColumn, Index, CreateDateColumn, UpdateDateColumn, JoinColumn, ManyToOne, OneToMany } from "typeorm";
import { ZONE_TYPES } from "../util/enums";
import { User } from "./User";
import { Model } from "./Model";
import { Subzone } from "./Subzone";
import { Zonetrait } from "./Zonetrait";

@Entity("zones")
@Index(["zurl", "user"], { unique: true })
export class Zone extends Model {

	@Column({
		name: "type",
	})
	type: ZONE_TYPES;
	
	@Column({
		name: "name",
	})
	name: string;

	@Column({
		name: "zurl",
	})
	zurl: string;
	
	@ManyToOne(type => User, {
		onDelete: "CASCADE",
	})
	@JoinColumn({
		name: "user_id",
	})
	user?: User | Model;

	@OneToMany(type => Subzone, subzone => subzone.zone)
	subzones?: Subzone[] | Model[];

	@OneToMany(type => Zonetrait, zonetrait => zonetrait.zone)
	zonetraits?: Zonetrait[] | Model[];

}