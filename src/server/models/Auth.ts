import _ from "lodash";
import { Entity, PrimaryColumn, Column, CreateDateColumn, UpdateDateColumn, OneToOne, JoinColumn } from "typeorm";
import { Model } from "./Model";
import { User } from "./User";
import { generatePasswordSalt, hashPassword, generateEmailVerificationHash } from "../util/auth";

@Entity("auths")
export class Auth extends Model {
	constructor(){
		super();
		this.salt = generatePasswordSalt();
	}

	@Column({
		name: "password",
	})
	_password: string;

	set password(pass: string){
		if(_.isUndefined(this.salt))
			throw new ReferenceError("Auth object does not contain a salt. This should have been generated on construction");

		this._password = hashPassword(pass, this.salt); 
	}

	get password(){
		return this._password;
	}

	@Column({
		name: "salt",
	})
	readonly salt: string;

	@Column({
		name: "last_login",
		default: () => "NOW()",
	})
	lastLogin?: Date;
	
	@OneToOne(type => User, user => user.auth, {
		onDelete: "CASCADE",
	})
	@JoinColumn({
		name: "user_id",
	})
	user?: User | Model;

	/**
	 * Validate a given password with the hashed password in the Auth object
	 * @param {string} password The password to validate against the hashed password
	 * @returns {boolean} true if the given password validates with the Auth's object password, false otherwise
	 */
	validatePassword(password: string): boolean {
		return this.password === hashPassword(password, this.salt);
	}
}