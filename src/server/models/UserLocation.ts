import { Entity, UpdateDateColumn, CreateDateColumn, PrimaryColumn, JoinColumn, ManyToOne, OneToOne, PrimaryGeneratedColumn } from "typeorm";
import { Location } from "./Location";
import { User } from "./User";
import { Model } from "./Model";

@Entity("user_locations")
export class UserLocation extends Model {

	@ManyToOne(type => User, user => user.locations)
	@JoinColumn({
		name: "user_id",
	})
	user?: User | Model;

	@ManyToOne(type => Location, location => location.users, {
		cascade: true,
	})
	@JoinColumn({
		name: "location_id",
	})
	location?: Location | Model;

}