import { Auth } from "../Auth";

describe("validatePassword", () => {
	test("validating the same password should return true", () => {
		const auth = new Auth();
		const password = "samePassword";
		auth.password = password;

		expect(auth.validatePassword(password)).toBe(true);
	});

	test("validating the wrong password or no password should return false", () => {
		const auth = new Auth();
		auth.password = "somepass";
		
		expect(auth.validatePassword("anotherpass")).toBe(false);
		expect(auth.validatePassword("")).toBe(false);
	});

	test("validating password on object with set password should return false", () => {
		const auth = new Auth();

		expect(auth.validatePassword("password")).toBe(false);
		expect(auth.validatePassword("")).toBe(false);
	})
});