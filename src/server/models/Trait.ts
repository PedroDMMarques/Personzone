import { Model } from "./Model";
import { Entity, Column } from "typeorm";

@Entity("traits")
export class Trait extends Model {

	@Column({
		name: "name",
	})
	name: string;

}