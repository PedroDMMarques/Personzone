import { Entity, CreateDateColumn, UpdateDateColumn, Column, PrimaryGeneratedColumn, ManyToMany, OneToMany } from "typeorm";
import { LOCATION_TYPES } from "../util/enums";
import { User } from "./User";
import { UserLocation } from "./UserLocation";
import { Model } from "./Model";

@Entity("locations")
export class Location extends Model {

	@Column({
		name: "type",
		default: LOCATION_TYPES.LOCATION,
	})
	type?: LOCATION_TYPES;
	
	@Column({
		name: "place_id"
	})
	placeId: string;
	
	@Column({
		name: "administrative_area_level_1",
		nullable: true,
	})
	administrativeAreaLevel1?: string;
	
	@Column({
		name: "administrative_area_level_2",
		nullable: true,
	})
	administrativeAreaLevel2?: string;
	
	@Column({
		name: "administrative_area_level_3",
		nullable: true,
	})
	administrativeAreaLevel3?: string;
	
	@Column({
		name: "country",
		nullable: true,
	})
	country?: string;
	
	@Column({
		name: "locality",
		nullable: true,
	})
	locality?: string;
	
	@Column({
		name: "route",
		nullable: true,
	})
	route?: string;

	@Column({
		name: "formatted_address",
		nullable: true,
	})
	formattedAddress?: string;
	
	@Column({
		name: "lat",
		type: "float",
		nullable: true,
	})
	lat?: number;

	@Column({
		name: "lng",
		type: "float",
		nullable: true,
	})
	lng?: number;

	@OneToMany(type => UserLocation, userLocation => userLocation.location)
	users?: UserLocation[] | Model[];

}