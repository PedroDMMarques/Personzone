import { Model } from "./Model";
import { Entity, Column, ManyToOne, JoinColumn } from "typeorm";
import { Subzone } from "./Subzone";
import { User } from "./User";

@Entity("comments")
export class Comment extends Model {

	@Column({
		name: "text",
		type: "text",
	})
	text: string;

	@Column({
		name: "thanked",
		default: false,
	})
	thanked?: boolean;

	@Column({
		name: "anonymous",
		default: true,
	})
	anonymous?: boolean;

	@ManyToOne(type => Subzone)
	@JoinColumn({
		name: "subzone_id",
	})
	subzone?: Subzone | Model;

	@ManyToOne(type => User, {
		onDelete: "SET NULL",
		nullable: true,
	})
	@JoinColumn({
		name: "user_id",
	})
	user?: User | Model;

}