import _ from "lodash";
import { PrimaryGeneratedColumn, CreateDateColumn, UpdateDateColumn } from "typeorm";

export abstract class Model {
	@PrimaryGeneratedColumn({
		name: "id",
	})
	id?: number;

	@CreateDateColumn({
		name: "created_at",
	})
	createdAt?: number;

	@UpdateDateColumn({
		name: "updated_at",
	})
	updatedAt?: number;

	/**
	 * @override
	 * Method used by JSON.stringify()
	 * Removes every variable with getters/setters (that starts with a "_") and replaced it with the normal name
	 * Example, because user.name has get/set, the resulting object becomes user.name instead of user._name
	 */
	toJSON() {
		const proto = Object.getPrototypeOf(this);
		const jsonObj: any = Object.assign({}, this);
		
		_.each(jsonObj, function(value, key){
			if(key[0] === "_")
				delete jsonObj[key];
		});

		_.each(Object.getOwnPropertyNames(proto), function(name: string){
			const desc = Object.getOwnPropertyDescriptor(proto, name);
			if(desc.get)
				jsonObj[name] = this[name];
			
		}.bind(this));

		return jsonObj;
	  }
}