import { Model } from "./Model";
import { Entity, Column, OneToMany, OneToOne, JoinColumn } from "typeorm";
import { PRIVACY_SETTINGS } from "../util/enums";
import { User } from "./User";

@Entity("user_privacies")
export class UserPrivacy extends Model {

	@Column({
		name: "join_date",
		default: PRIVACY_SETTINGS.PRIVATE
	})
	joinDate?: PRIVACY_SETTINGS;

	@Column({
		name: "gender",
		default: PRIVACY_SETTINGS.PRIVATE
	})
	gender?: PRIVACY_SETTINGS;

	@Column({
		name: "dob",
		default: PRIVACY_SETTINGS.PRIVATE
	})
	dob?: PRIVACY_SETTINGS;

	@Column({
		name: "blood_type",
		default: PRIVACY_SETTINGS.PRIVATE
	})
	bloodType?: PRIVACY_SETTINGS;

	@Column({
		name: "driving_license",
		default: PRIVACY_SETTINGS.PRIVATE
	})
	drivingLicense?: PRIVACY_SETTINGS;

	@Column({
		name: "seeking_work",
		default: PRIVACY_SETTINGS.PRIVATE
	})
	seekingWork?: PRIVACY_SETTINGS;

	@Column({
		name: "rel_status",
		default: PRIVACY_SETTINGS.PRIVATE
	})
	relStatus?: PRIVACY_SETTINGS;

	@Column({
		name: "def_word",
		default: PRIVACY_SETTINGS.PRIVATE
	})
	defWord?: PRIVACY_SETTINGS;

	@OneToOne(type => User, user => user.privacy, {
		onDelete: "CASCADE",
	})
	@JoinColumn({
		name: "user_id",
	})
	user?: User | Model;

}