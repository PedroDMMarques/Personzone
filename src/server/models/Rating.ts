import { Model } from "./Model";
import { Entity, Column, ManyToOne, JoinColumn } from "typeorm";
import { Zonetrait } from "./Zonetrait";
import { User } from "./User";

@Entity("ratings")
export class Rating extends Model {

	@Column({
		name: "rating",
	})
	rating: number;

	@Column({
		name: "thanked",
		default: false,
	})
	thanked?: boolean;

	@ManyToOne(type => Zonetrait)
	@JoinColumn({
		name: "zonetrait_id",
	})
	zonetrait?: Zonetrait | Model;

	@ManyToOne(type => User, {
		onDelete: "CASCADE",
	})
	@JoinColumn({
		name: "user_id",
	})
	user?: User | Model;

}