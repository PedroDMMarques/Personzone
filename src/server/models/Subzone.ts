import { Model } from "./Model";
import { Entity, Column, ManyToOne, JoinColumn, OneToMany, Index } from "typeorm";
import { Zone } from "./Zone";
import { Comment } from "./Comment";

@Entity("subzones")
@Index(["name", "zone"], { unique: true })
export class Subzone extends Model {

	@Column({
		name: "name",
	})
	name: string;

	@ManyToOne(type => Zone)
	@JoinColumn({
		name: "zone_id",
	})
	zone?: Zone | Model;

	@OneToMany(type => Comment, comment => comment.subzone)
	comments?: Comment[] | Model[];

}