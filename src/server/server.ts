import express, { NextFunction } from "express";

import fs from "fs";

import cookieParser from "cookie-parser";
import bodyParser from "body-parser";
import expressSession from "express-session";

import passport from "passport";
import { configPassport } from "./config/passport";

import _ from "lodash";
import { createDatabase } from "./database";
import { createExpressDB } from "./middleware/expressDB";
import { createErrorHandler } from "./middleware/errorHandler";
import { createMailer } from "./middleware/mailer";
import { createBasicAuth } from "./middleware/basicAuth";
import { createLocalizer } from "./middleware/localizer";
import { loadEnvironment } from "./util/node";
import path from "path";
import winston from "winston";

import { createAppRoutes } from "./controllers/app";
import { createApiRoutes } from "./controllers/api";
import { createDevRoutes } from "./controllers/dev";

loadEnvironment([
	"SERVER_PORT",
	"SERVER_SESSION_SECRET",
	"DATABASE_HOST",
	"DATABASE_PORT",
	"DATABASE_USER",
	"DATABASE_PASS",
	"DATABASE_NAME",
	"MAILER_HOST",
	"MAILER_PORT",
	"MAILER_USER",
	"MAILER_PASS",
]);

winston.info(`Launching server in ${process.env.NODE_ENV} mode`);

async function main() {
	try{
		const server = express();
		const db = await createDatabase({
			host: process.env.DATABASE_HOST,
			port: _.toNumber(process.env.DATABASE_PORT),
			user: process.env.DATABASE_USER,
			pass: process.env.DATABASE_PASS,
			database: process.env.DATABASE_NAME,
		});
		
		server.set("views", path.join(__dirname, "..", "..", "views"));
		server.set("view engine", "pug");
		server.use("/public", express.static(path.join(__dirname, "..", "..", "public")));
		
		server.use(cookieParser());
		server.use(bodyParser.json());
		server.use(bodyParser.urlencoded({ extended: true }));
		
		server.use(expressSession({
			secret: process.env.SERVER_SESSION_SECRET,
			resave: false,
			saveUninitialized: true,
		}));

		const basicAuthPath = path.join(__dirname, "..", "..", ".basicauth");
		if(fs.existsSync(basicAuthPath)){
			winston.info(`Detected .basicauth file, setting up basic-authentication`);
			server.use(createBasicAuth({
				realm: "Personzone",
				basicAuthFile: basicAuthPath,
			}));
		}

		server.use(createExpressDB({connection: db}));
		server.use(await createMailer({
			host: process.env.MAILER_HOST,
			port: _.toNumber(process.env.MAILER_PORT),
			user: process.env.MAILER_USER,
			pass: process.env.MAILER_PASS,
			defaults: {
				from: `Personzone ${process.env.MAILER_USER}`,
			}
		}));

		server.use(createLocalizer({
			defaultLocale: "en",
			localeDir: path.join(__dirname, "..", "..", "locales"),
		}));

		server.use(passport.initialize());
		server.use(passport.session());
		await configPassport(db);

		server.use("/api", createApiRoutes());
		server.use("/dev", createDevRoutes());
		server.use("/", createAppRoutes());

		server.use(createErrorHandler({}));
		server.listen(_.toNumber(process.env.SERVER_PORT), function () {
			winston.info(`Server running and listening on port ${process.env.SERVER_PORT}`);
		});
	}catch(e){
		console.log(e);
	}
}

main();