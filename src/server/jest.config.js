module.exports = {
	"roots": [
		"<rootDir>",
	],

	"transform": {
		"^.+\\.tsx?$": "ts-jest",
	},

	// Test everything inside a '__tests__' folder or any file with (.test|.spec)(.ts|.tsx|.js|.jsx)
	"testRegex": "(/__tests__/.*|(\\.|/)(test|spec))\\.(jsx?|tsx?)$",
	"moduleFileExtensions": [
		"ts",
		"tsx",
		"js",
		"jsx",
		"json",
		"node",
	],

	
	"globals": {
		"ts-jest": {
			"tsConfigFile": "tsconfig.json",

			// This needs to be here for now to test .ts on the fly instead of transpiling it to .js first
			// Look at relevant github issue (https://github.com/kulshekhar/ts-jest/issues/439)
			"useExperimentalLanguageServer": true,
		},
	},
}