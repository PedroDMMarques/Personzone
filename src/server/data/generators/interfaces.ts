import { Weight } from "../../util/math";
import { GENDERS, BLOOD_TYPES, REL_STATUSES, PRIVACY_SETTINGS, ZONE_TYPES } from "../../util/enums";
import UserGenerator from "./UserGenerator";
import AuthGenerator from "./AuthGenerator";
import ZoneGenerator from "./ZoneGenerator";
import LocationGenerator from "./LocationGenerator";
import UserPrivacyGenerator from "./UserPrivacyGenerator";
import ZonetraitGenerator from "./ZonetraitGenerator";
import SubzoneGenerator from "./SubzoneGenerator";
import RatingGenerator from "./RatingGenerator";
import CommentGenerator from "./CommentGenerator";
import UserEmailGenerator from "./UserEmailGenerator";

export interface Generators {
	user: UserGenerator
	auth: AuthGenerator
	zone: ZoneGenerator
	location: LocationGenerator
	userPrivacy: UserPrivacyGenerator
	userEmail: UserEmailGenerator
	zonetrait: ZonetraitGenerator
	subzone: SubzoneGenerator
	rating: RatingGenerator
	comment: CommentGenerator
}

/**
 * Configuration object for initialising all the generators
 */
export interface GeneratorsConfig {
	user: UserGeneratorConfig
	auth: AuthGeneratorConfig
	zone: ZoneGeneratorConfig
	location: LocationGeneratorConfig
	zonetrait: ZonetraitGeneratorConfig
	subzone: SubzoneGeneratorConfig
	rating: RatingGeneratorConfig
	comment: CommentGeneratorConfig
	userPrivacy: UserPrivacyGeneratorConfig
	userEmail: UserEmailGeneratorConfig
}

/**
 * Configuration object for a single Generator
 */
export interface GeneratorConfig {
	dataPaths?: {[name: string]: string}
	parameters?: object
	probabilities?: {
		[name: string]: Weight<any>[]
	}
}

/**
 * Configuration object for the UserEmailGenerator
 */
export interface UserEmailGeneratorConfig extends GeneratorConfig {
	probabilities: {
		verified: Weight<boolean>[]
		privacy: Weight<PRIVACY_SETTINGS>[]
		emailDomain: Weight<string>[]
	}
}

/**
 * Configuration object for the AuthGenerator
 */
export interface AuthGeneratorConfig extends GeneratorConfig {
	parameters: {
		password: string
	},
}

/**
 * Configuration object for the LocationGenerator
 */
export interface LocationGeneratorConfig extends GeneratorConfig {
	dataPaths: {
		locations: string
	}
}

/**
 * Configuration object for the CommentGenerator
 */
export interface CommentGeneratorConfig extends GeneratorConfig {
	dataPaths: {
		comments: string
	}

	probabilities: {
		thanked: Weight<boolean>[]
		anonymous: Weight<boolean>[]
	}
}

/**
 * Configuration object for the RatingGenerator
 */
export interface RatingGeneratorConfig extends GeneratorConfig {

}

/**
 * Configuration object for the ZonetraitGenerator
 */
export interface ZonetraitGeneratorConfig extends GeneratorConfig {

}

/**
 * Configuration object for the SubzoneGenerator
 */
export interface SubzoneGeneratorConfig extends GeneratorConfig {
	dataPaths: {
		names: string
	}
}

/**
 * Configuration object for the UserPrivacyGeneratorConfig
 */
export interface UserPrivacyGeneratorConfig extends GeneratorConfig {
	probabilities: {
		joinDate: Weight<PRIVACY_SETTINGS>[]
		gender: Weight<PRIVACY_SETTINGS>[]
		dob: Weight<PRIVACY_SETTINGS>[]
		bloodType: Weight<PRIVACY_SETTINGS>[]
		drivingLicense: Weight<PRIVACY_SETTINGS>[]
		seekingWork: Weight<PRIVACY_SETTINGS>[]
		relStatus: Weight<PRIVACY_SETTINGS>[]
		defWord: Weight<PRIVACY_SETTINGS>[]
	}
}

/**
 * Configuration object for the ZoneGenerator
 */
export interface ZoneGeneratorConfig extends GeneratorConfig {
	dataPaths: {
		names: string
	}

	probabilities: {
		type: Weight<ZONE_TYPES>[]
	}
}

/**
 * Configuration object for the UserGenerator
 */
export interface UserGeneratorConfig extends GeneratorConfig {
	dataPaths: {
		maleFirstNames: string
		femaleFirstNames: string
		lastNames: string
		defWords: string
	}

	parameters: {
		startDobYear: number
		endDobYear: number
	}
	
	probabilities: {
		dob: Weight<boolean>[],
		gender: Weight<GENDERS>[],
		defWord: Weight<boolean>[],
		bloodType: Weight<BLOOD_TYPES>[]
		relStatus: Weight<REL_STATUSES>[]
		drivingLicense: Weight<boolean>[]
		seekingWork: Weight<boolean>[]
		numberZones: Weight<number>[]
		numberLocations: Weight<number>[]
		numberFirstNames: Weight<number>[]
		numberLastNames: Weight<number>[]
		numberEmails: Weight<number>[]
	}
}