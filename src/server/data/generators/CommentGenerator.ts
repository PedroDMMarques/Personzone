import _ from "lodash";
import Generator from "./Generator";
import { CommentGeneratorConfig } from "./interfaces";
import { weightedRNG } from "../../util/math";
import { Comment } from "../../models/Comment";
import { User } from "../../models/User";

interface GenerateOptions {
	userIds?: number[]
}

export default class CommentGenerator extends Generator {
	constructor(public config: CommentGeneratorConfig){
		super(config);
	}

	generate({
		userIds
	}: GenerateOptions): Comment {
		if(_.isEmpty(userIds))
			return null;
		
		const p = this.config.probabilities;

		let comment = new Comment();
		comment.text = this.data.comments[_.random(this.data.comments.length - 1)];
		comment.thanked = weightedRNG(p.thanked);
		comment.anonymous = weightedRNG(p.anonymous);
		
		const user: User = new User();
		user.id = userIds[_.random(userIds.length-1)];
		comment.user = user;
		
		return comment;
	}
}