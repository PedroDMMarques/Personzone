import _ from "lodash";
import Generator from "./Generator";
import { UserPrivacyGeneratorConfig } from "./interfaces";
import { UserPrivacy } from "../../models/UserPrivacy";
import { weightedRNG } from "../../util/math";

interface GenerateOptions {

}

export default class UserPrivacyGenerator extends Generator {
	constructor(public config: UserPrivacyGeneratorConfig){
		super(config);
	}

	generate({}: GenerateOptions): UserPrivacy {
		const p = this.config.probabilities;
		let privacy = new UserPrivacy();
		privacy.joinDate = weightedRNG(p.joinDate);
		privacy.gender = weightedRNG(p.gender);
		privacy.dob = weightedRNG(p.dob);
		privacy.bloodType = weightedRNG(p.bloodType);
		privacy.drivingLicense = weightedRNG(p.drivingLicense);
		privacy.seekingWork = weightedRNG(p.seekingWork);
		privacy.relStatus = weightedRNG(p.relStatus);
		privacy.defWord = weightedRNG(p.defWord);

		return privacy;
	}
}