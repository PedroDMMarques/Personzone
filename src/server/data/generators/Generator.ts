import _ from "lodash";
import { GeneratorConfig, Generators } from "./interfaces";

export default abstract class Generator {
	public generators: Generators;
	public data: {[name: string] : any} = {};
	
	protected constructor(public config: GeneratorConfig){
		_.each(config.dataPaths, (path, key) => {
			this.data[key] = require(path);
		});
	}

	abstract generate(options: object) : object;
}