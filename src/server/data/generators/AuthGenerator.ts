import _ from "lodash";
import Generator from "./Generator";
import { AuthGeneratorConfig } from "./interfaces";
import { Auth } from "../../models/Auth";
import { weightedRNG } from "../../util/math";
import { generatePasswordSalt, generateEmailVerificationHash, hashPassword } from "../../util/auth";

interface GenerateOptions {

}

export default class AuthGenerator extends Generator {
	constructor(public config: AuthGeneratorConfig){
		super(config);
	}

	generate({}: GenerateOptions): Auth {
		const p = this.config.probabilities;
		let auth = new Auth();
		auth.password = hashPassword(this.config.parameters.password, auth.salt);
		
		return auth;
	}
}