import _ from "lodash";
import Generator from "./Generator";
import { RatingGeneratorConfig } from "./interfaces";
import { Rating } from "../../models/Rating";
import { User } from "../../models/User";

interface GenerateOptions {
	userIds?: number[]
}

export default class RatingGenerator extends Generator {
	constructor(public config: RatingGeneratorConfig){
		super(config);
	}

	generate({
		userIds
	}: GenerateOptions): Rating {
		if(_.isEmpty(userIds))
			return null;

		let rating = new Rating();
		rating.rating = _.random(1, 5);

		const user: User = new User();
		user.id = userIds[_.random(userIds.length-1)];
		rating.user = user;

		return rating;
	}
}