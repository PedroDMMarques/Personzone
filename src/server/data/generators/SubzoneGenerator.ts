import _ from "lodash";
import Generator from "./Generator";
import { SubzoneGeneratorConfig } from "./interfaces";
import { Subzone } from "../../models/Subzone";

interface GenerateOptions {
	usedNames?: string[]
}

export default class SubzoneGenerator extends Generator {
	constructor(public config: SubzoneGeneratorConfig){
		super(config);
	}

	generate({
		usedNames
	}: GenerateOptions): Subzone {
		const name = this.generateName(usedNames);
		if(_.isNil(name))
			return null;

		let subzone = new Subzone();
		subzone.name = name;

		return subzone;
	}

	/**
	 * Generate a name
	 * @private
	 * @param {string[]} usedNames An array of already used names that the new name should not be
	 * @returns {string} the generated name
	 */
	private generateName(usedNames: string[] = []): string {
		const usableNames = _.without(this.data.names, ...usedNames);
		return usableNames[_.random(usableNames.length-1)];
	}
}