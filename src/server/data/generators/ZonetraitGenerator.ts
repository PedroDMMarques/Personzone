import _ from "lodash";
import Generator from "./Generator";
import { ZonetraitGeneratorConfig } from "./interfaces";
import { Zonetrait } from "../../models/Zonetrait";
import { Trait } from "../../models/Trait";

interface GenerateOptions {
	traitIds?: number[]
}

export default class ZonetraitGenerator extends Generator {
	constructor(public config: ZonetraitGeneratorConfig){
		super(config);
	}

	generate({
		traitIds,
	}: GenerateOptions): Zonetrait {
		if(_.isEmpty(traitIds))
			return null;

		let zonetrait = new Zonetrait();
		
		const trait: Trait = new Trait();
		trait.id = traitIds[_.random(traitIds.length-1)];
		zonetrait.trait = trait;

		return zonetrait;
	}
}