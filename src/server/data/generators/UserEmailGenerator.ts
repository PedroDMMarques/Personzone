import _ from "lodash";
import Generator from "./Generator";
import { UserEmailGeneratorConfig } from "./interfaces";
import { weightedRNG } from "../../util/math";
import { generatePasswordSalt, generateEmailVerificationHash, hashPassword } from "../../util/auth";
import { UserEmail } from "../../models/UserEmail";

interface GenerateOptions {
	/**
	 * If supplied will use this has the user of the email (before the @)
	 */
	base?: string

	/**
	 * Whether the email should be generated as primary or not
	 * Defaults to false
	 */
	primary?: boolean
}

export default class UserEmailGenerator extends Generator {
	constructor(public config: UserEmailGeneratorConfig){
		super(config);
	}

	generate({base, primary}: GenerateOptions): UserEmail {
		const p = this.config.probabilities;
		let email = new UserEmail();
		email.verified = weightedRNG(p.verified);
		email.privacy = weightedRNG(p.privacy);
		email.email = this.generateEmail(base);
		email.primary = _.isUndefined(primary) ? false : primary;

		return email;
	}

	/**
	 * Generate the email
	 * @param {string} [base] If supplied will use this as the user of the email (before the @), otherwise will generate that as well
	 * @returns {string} the generated email
	 */
	private generateEmail(base?: string): string {
		const emailDomain = weightedRNG(this.config.probabilities.emailDomain);
		if(!base)
			base = generateEmailVerificationHash();

		return `${base}@${emailDomain}`;
	}
}