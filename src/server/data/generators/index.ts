import _ from "lodash";
import { Generators, GeneratorsConfig } from "./interfaces";
import UserGenerator from "./UserGenerator";
import LocationGenerator from "./LocationGenerator";
import UserPrivacyGenerator from "./UserPrivacyGenerator";
import AuthGenerator from "./AuthGenerator";
import ZoneGenerator from "./ZoneGenerator";
import RatingGenerator from "./RatingGenerator";
import ZonetraitGenerator from "./ZonetraitGenerator";
import SubzoneGenerator from "./SubzoneGenerator";
import CommentGenerator from "./CommentGenerator";
import UserEmailGenerator from "./UserEmailGenerator";

export function createGenerators(config: GeneratorsConfig): Generators {
	let generators: Generators = {
		user: new UserGenerator(config.user),
		location: new LocationGenerator(config.location),
		userPrivacy: new UserPrivacyGenerator(config.userPrivacy),
		userEmail: new UserEmailGenerator(config.userEmail),
		auth: new AuthGenerator(config.auth),
		zone: new ZoneGenerator(config.zone),
		rating: new RatingGenerator(config.rating),
		zonetrait: new ZonetraitGenerator(config.zonetrait),
		subzone: new SubzoneGenerator(config.subzone),
		comment: new CommentGenerator(config.comment),
	};

	_.each(generators, g => {
		g.generators = generators;
	});

	return generators;
}