import _ from "lodash";
import Generator from "./Generator";
import { LocationGeneratorConfig } from "./interfaces";
import { Location } from "../../models/Location";

interface GenerateOptions {

}

export default class LocationGenerator extends Generator {
	constructor(public config: LocationGeneratorConfig){
		super(config);
	}

	generate({}: GenerateOptions): Location {
		const locationBits: {[name: string]: any} = this.data.locations[_.random(this.data.locations.length-1)];
		let location = new Location();
		location.placeId = locationBits.placeId;
		location.administrativeAreaLevel1 = locationBits.administrativeAreaLevel1;
		location.administrativeAreaLevel2 = locationBits.administrativeAreaLevel2;
		location.administrativeAreaLevel3 = locationBits.administrativeAreaLevel3;
		location.country = locationBits.country;
		location.locality = locationBits.locality;
		location.route = locationBits.route;
		location.lat = locationBits.point.coordinates[0];
		location.lng = locationBits.point.coordinates[1];

		return location;
	}
}