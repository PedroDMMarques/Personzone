import path from "path";
import { GeneratorsConfig, RatingGeneratorConfig, ZonetraitGeneratorConfig, AuthGeneratorConfig, UserPrivacyGeneratorConfig, ZoneGeneratorConfig, SubzoneGeneratorConfig, CommentGeneratorConfig, LocationGeneratorConfig, UserGeneratorConfig, UserEmailGeneratorConfig } from "../interfaces";
import { GENDERS, BLOOD_TYPES, REL_STATUSES, PRIVACY_SETTINGS, ZONE_TYPES } from "../../../util/enums";

const ratingConfig: RatingGeneratorConfig = {};
const zonetraitConfig: ZonetraitGeneratorConfig = {};
const authConfig: AuthGeneratorConfig = {
	parameters: {
		password: "pass"
	}
}
const userEmailConfig: UserEmailGeneratorConfig = {
	probabilities: {
		emailDomain: [{
			value: "yopmail",
			probability: 50,
		},{
			value: "mailinator.com",
			probability: 50,
		}],

		verified: [{
			value: true,
			probability: 90,
		}, {
			value: false,
			probability: 10,
		}],

		privacy: [{
			value: PRIVACY_SETTINGS.PRIVATE,
			probability: 33,
		},{
			value: PRIVACY_SETTINGS.PUBLIC,
			probability: 33,
		},{
			value: PRIVACY_SETTINGS.FRIENDS,
			probability: 33,
		}]
	}
}
const userPrivacyConfig: UserPrivacyGeneratorConfig = {
	probabilities: {
		joinDate: [{
			value: PRIVACY_SETTINGS.PRIVATE,
			probability: 33,
		},{
			value: PRIVACY_SETTINGS.PUBLIC,
			probability: 33,
		},{
			value: PRIVACY_SETTINGS.FRIENDS,
			probability: 33,
		}],

		gender: [{
			value: PRIVACY_SETTINGS.PRIVATE,
			probability: 33,
		},{
			value: PRIVACY_SETTINGS.PUBLIC,
			probability: 33,
		},{
			value: PRIVACY_SETTINGS.FRIENDS,
			probability: 33,
		}],

		dob: [{
			value: PRIVACY_SETTINGS.PRIVATE,
			probability: 33,
		},{
			value: PRIVACY_SETTINGS.PUBLIC,
			probability: 33,
		},{
			value: PRIVACY_SETTINGS.FRIENDS,
			probability: 33,
		}],

		bloodType: [{
			value: PRIVACY_SETTINGS.PRIVATE,
			probability: 33,
		},{
			value: PRIVACY_SETTINGS.PUBLIC,
			probability: 33,
		},{
			value: PRIVACY_SETTINGS.FRIENDS,
			probability: 33,
		}],

		drivingLicense: [{
			value: PRIVACY_SETTINGS.PRIVATE,
			probability: 33,
		},{
			value: PRIVACY_SETTINGS.PUBLIC,
			probability: 33,
		},{
			value: PRIVACY_SETTINGS.FRIENDS,
			probability: 33,
		}],

		seekingWork: [{
			value: PRIVACY_SETTINGS.PRIVATE,
			probability: 33,
		},{
			value: PRIVACY_SETTINGS.PUBLIC,
			probability: 33,
		},{
			value: PRIVACY_SETTINGS.FRIENDS,
			probability: 33,
		}],

		relStatus: [{
			value: PRIVACY_SETTINGS.PRIVATE,
			probability: 33,
		},{
			value: PRIVACY_SETTINGS.PUBLIC,
			probability: 33,
		},{
			value: PRIVACY_SETTINGS.FRIENDS,
			probability: 33,
		}],

		defWord: [{
			value: PRIVACY_SETTINGS.PRIVATE,
			probability: 33,
		},{
			value: PRIVACY_SETTINGS.PUBLIC,
			probability: 33,
		},{
			value: PRIVACY_SETTINGS.FRIENDS,
			probability: 33,
		}],
	}
}
const zoneConfig: ZoneGeneratorConfig = {
	dataPaths: {
		names: path.join(process.cwd(), "data", "zoneNames")
	},

	probabilities: {
		type: [{
			value: ZONE_TYPES.PERSONAL,
			probability: 90,
		},{
			value: ZONE_TYPES.CUSTOM,
			probability: 10,
		}],
	},
}
const subzoneConfig: SubzoneGeneratorConfig = {
	dataPaths: {
		names: path.join(process.cwd(), "data", "subzoneNames")
	}
}
const commentConfig: CommentGeneratorConfig = {
	dataPaths: {
		comments: path.join(process.cwd(), "data", "jokes")
	},

	probabilities: {
		thanked: [{
			value: true,
			probability: 10,
		},{
			value: false,
			probability: 90,
		}],

		anonymous: [{
			value: true,
			probability: 98,
		},{
			value: false,
			probability: 2,
		}]
	}
}
const locationConfig: LocationGeneratorConfig = {
	dataPaths: {
		locations: path.join(process.cwd(), "data", "locations")
	}
}
const userConfig: UserGeneratorConfig = {
	dataPaths: {
		maleFirstNames: path.join(process.cwd(), "data", "maleFirstNames"),
		femaleFirstNames: path.join(process.cwd(), "data", "femaleFirstNames"),
		lastNames: path.join(process.cwd(), "data", "lastNames"),
		defWords: path.join(process.cwd(), "data", "defWords"),
	},

	parameters: {
		startDobYear: 1975,
		endDobYear: 2005,
	},

	probabilities: {
		dob: [{
			value: true,
			probability: 50,
		},{
			value: false,
			probability: 50,
		}],

		gender: [{
			value: GENDERS.M,
			probability: 45,
		},{
			value: GENDERS.F,
			probability: 45,
		},{
			value: GENDERS.O,
			probability: 5,
		},{
			value: null,
			probability: 5,
		}],

		defWord: [{
			value: true,
			probability: 50,
		}, {
			value: false,
			probability: 50,
		}],

		bloodType: [{
			value: null,
			probability: 10,
		},{
			value: BLOOD_TYPES.A_MINUS,
			probability: 1,
		},{
			value: BLOOD_TYPES.A_PLUS,
			probability: 1,
		},{
			value: BLOOD_TYPES.AB_MINUS,
			probability: 1,
		},{
			value: BLOOD_TYPES.AB_PLUS,
			probability: 1,
		},{
			value: BLOOD_TYPES.B_MINUS,
			probability: 1,
		},{
			value: BLOOD_TYPES.B_PLUS,
			probability: 1,
		},{
			value: BLOOD_TYPES.O_MINUS,
			probability: 1,
		},{
			value: BLOOD_TYPES.O_PLUS,
			probability: 1,
		}],

		relStatus: [{
			value: null,
			probability: 40,
		},{
			value: REL_STATUSES.SINGLE,
			probability: 30,
		},{
			value: REL_STATUSES.IN_RELATION,
			probability: 25,
		},{
			value: REL_STATUSES.ENGAGED,
			probability: 5,
		},{
			value: REL_STATUSES.MARRIED,
			probability: 18,
		},{
			value: REL_STATUSES.COMPLICATED,
			probability: 2,
		},{
			value: REL_STATUSES.OPEN,
			probability: 3,
		},{
			value: REL_STATUSES.WIDOW,
			probability: 10,
		},{
			value: REL_STATUSES.SEPARATED,
			probability: 12,
		},{
			value: REL_STATUSES.DIVORCED,
			probability: 20,
		},{
			value: REL_STATUSES.CIVIL_UNION,
			probability: 1,
		}],

		drivingLicense: [{
			value: null,
			probability: 30,
		},{
			value: false,
			probability: 30,
		},{
			value: true,
			probability: 40,
		}],

		seekingWork: [{
			value: null,
			probability: 40,
		},{
			value: true,
			probability: 40,
		},{
			value: false,
			probability: 30,
		}],

		numberZones: [{
			value: 1,
			probability: 80,
		},{
			value: 2,
			probability: 10,
		},{
			value: 3,
			probability: 10
		}],

		numberFirstNames: [{
			value: 1,
			probability: 85,
		},{
			value: 2,
			probability: 13,
		},{
			value: 3,
			probability: 2,
		}],

		numberLastNames: [{
			value: 1,
			probability: 90,
		},{
			value: 2,
			probability: 10,
		}],

		numberLocations: [{
			value: 1,
			probability: 100,
		}],

		numberEmails: [{
			value: 1,
			probability: 80,
		}, {
			value: 2,
			probability: 18
		},{
			value: 3,
			probability: 2
		}]
	}
}
const config: GeneratorsConfig = {
	rating: ratingConfig,
	zonetrait: zonetraitConfig,
	auth: authConfig,
	userPrivacy: userPrivacyConfig,
	userEmail: userEmailConfig,
	zone: zoneConfig,
	subzone: subzoneConfig,
	comment: commentConfig,
	location: locationConfig,
	user: userConfig,
}

export = config;