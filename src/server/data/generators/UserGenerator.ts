import _ from "lodash";
import Generator from "./Generator";
import { UserGeneratorConfig } from "./interfaces";
import { User } from "../../models/User";
import { GENDERS } from "../../util/enums";
import { weightedRNG, randomDate } from "../../util/math";
import { generatePurl } from "../../util/models";
import ZoneGenerator from "./ZoneGenerator";
import { UserLocation } from "../../models/UserLocation";

interface GenerateOptions {
	genAuth?: boolean
	genUserPrivacy?: boolean
	genZones?: boolean
	genLocations?: boolean
}

export default class UserGenerator extends Generator {
	constructor(public config: UserGeneratorConfig){
		super(config);
	}

	public generate({
		genAuth=true, genUserPrivacy=true,
		genZones=true, genLocations=true
	}: GenerateOptions): User {
		const p = this.config.probabilities;

		let user = new User();
		user.gender = weightedRNG(p.gender);
		user.name = this.generateName(user.gender);
		user.purl = generatePurl(user.name);
		user.dob = weightedRNG(p.dob) ? this.generateDob() : null;
		user.bloodType = weightedRNG(p.bloodType);
		user.relStatus = weightedRNG(p.relStatus);
		user.drivingLicense = weightedRNG(p.drivingLicense);
		user.seekingWork = weightedRNG(p.seekingWork);
		user.defWord = weightedRNG(p.defWord) ? this.generateDefWord() : null;

		if(genAuth)
			user.auth = this.generators.auth.generate({});

		if(genUserPrivacy)
			user.privacy = this.generators.userPrivacy.generate({});

		if(genLocations){
			user.locations = _.map(_.range(weightedRNG(p.numberLocations)), (v, index) => {
				let userLocation = new UserLocation();
				userLocation.user = user;
				userLocation.location = this.generators.location.generate({});
				return userLocation;
			});
		}

		if(genZones){
			user.zones = _.map(_.range(weightedRNG(p.numberZones)), (v, index) => {
				return this.generators.zone.generate({personal: index === 0, noPersonal: index !== 0});
			});
		}

		user.emails = _.map(_.range(weightedRNG(p.numberEmails)), (v, index) => {
			return this.generators.userEmail.generate({base: index === 0 ? user.purl : null, primary: index === 0});
		});

		return user;
	}

	/**
	 * Generate a random defining word
	 * @returns {string} the generated defining word
	 */
	private generateDefWord(): string{
		return this.data.defWords[_.random(this.data.defWords.length - 1)];
	}

	/**
	 * Generate a random date of birth
	 * @returns {Date} the generated date
	 */
	private generateDob(): Date{
		const start = new Date(this.config.parameters.startDobYear, 0, 1);
		const end = new Date(this.config.parameters.endDobYear, 0, 1);

		return randomDate(start, end);
	}

	/**
	 * Generate a name based on a supplied gender
	 * @private
	 * @param {GENDERS} gender The gender to generated the name based on
	 * @returns {string} the generated name 
	 */
	private generateName(gender: GENDERS): string {
		let firstNames: string[] = [];
		let lastNames: string[] = this.data.lastNames;

		if(gender === GENDERS.M)
			firstNames = this.data.maleFirstNames;
		else if(gender === GENDERS.F)
			firstNames = this.data.femaleFirstNames;
		else
			firstNames = _.random() ? this.data.maleFirstNames : this.data.femaleFirstNames;
		
		const nFirstNames = weightedRNG(this.config.probabilities.numberFirstNames);
		const nLastNames = weightedRNG(this.config.probabilities.numberLastNames);

		const firstName = _.join(_.map(_.range(nFirstNames), () => firstNames[_.random(firstNames.length -1 )]), " ");
		const lastName = _.join(_.map(_.range(nLastNames), () => lastNames[_.random(lastNames.length -1 )]), " ");
		
		return `${firstName} ${lastName}`;
	}
}