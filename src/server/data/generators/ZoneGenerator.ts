import _ from "lodash";
import Generator from "./Generator";
import { ZoneGeneratorConfig } from "./interfaces";
import { Zone } from "../../models/Zone";
import { ZONE_TYPES } from "../../util/enums";
import { weightedRNG } from "../../util/math";
import { generateZurl } from "../../util/models";

interface GenerateOptions {
	personal?: boolean
	noPersonal?: boolean
}

export default class ZoneGenerator extends Generator {
	constructor(public config: ZoneGeneratorConfig){
		super(config);
	}

	generate({
		personal=false,
		noPersonal=false
	}: GenerateOptions): Zone {
		const typeProbabilities = noPersonal ?
			_.filter(this.config.probabilities.type, (p) => p.value !== ZONE_TYPES.PERSONAL) :
			this.config.probabilities.type;
		
		let zone = new Zone();
		zone.type = personal ? ZONE_TYPES.PERSONAL : weightedRNG(typeProbabilities);
		zone.name = this.generateName(zone.type);
		zone.zurl = generateZurl(zone.name);

		return zone;
	}

	/**
	 * Generate a name based on the type of zone
	 * @private
	 * @param {ZONE_TYPES} type The type of the zone
	 * @returns {string} the generated name
	 */
	private generateName(type: ZONE_TYPES): string {
		if(type === ZONE_TYPES.PERSONAL)
			return "Personal";
		else
			return this.data.names[_.random(this.data.names.length-1)];
	}
}