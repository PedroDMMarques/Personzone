import _ from "lodash";
import inquirer from "inquirer";
import winston from "winston";
import { Subroutine } from "./db-pop";
import { ProcessBatchFn, processBatches } from "../util/async";
import { Connection } from "typeorm";
import { Zone } from "../models/Zone";
import { Subzone } from "../models/Subzone";

const BATCH_SIZE = 1000;

async function getOptions(): Promise<{min: number, max: number}>{
	const answers: {min?: number | string, max?: number | string} = await inquirer.prompt([{
		name: "min",
		type: "input",
		message: "Minimum number of subzones to generate?",
		default: 0,
	},{
		name: "max",
		type: "input",
		message: "Maximum number of subzones to generate?",
		default: 15,
	}]);

	return {
		min: _.toNumber(answers.min),
		max: _.toNumber(answers.max),
	}
}

async function countZones(db: Connection): Promise<number> {
	const obj = await db.getRepository(Zone)
		.createQueryBuilder()
		.select("COUNT(*)", "count")
		.getRawOne();

	return obj.count;
};

const main: Subroutine = async function(db, generators){
	const [options, nZones]: [{min: number, max: number}, number] = await Promise.all([
		getOptions(),
		countZones(db)
	]);

	if(nZones <= 0)
		winston.info("No zones found to generate subzones for");

	let subzonesGenerated: number = 0;
	const nBatches: number = _.ceil(nZones / BATCH_SIZE);
	const batchFn: ProcessBatchFn = async function(batch){
		const zones = await db.getRepository(Zone)
			.find({
				select: ["id"],
				relations: ["subzones"],
				take: BATCH_SIZE,
				skip: BATCH_SIZE * batch,
			});

		let subzones: Subzone[] = [];
		_.each(zones, function(zone){
			
			let usedNames = _.map(zone.subzones, function(subzone: Subzone){
				return subzone.name;
			});

			_.each(_.range(_.random(options.min, options.max)), function(){
				let subzone = generators.subzone.generate({usedNames});
				if(!subzone) return false;
				
				subzone.zone = zone;
				usedNames.push(subzone.name);
				subzones.push(subzone);
				subzonesGenerated++;
				return true;
			});

		});

		await db.transaction(async trans => {
			await trans.save(subzones);
		});

	}

	await processBatches(nBatches, batchFn, {printBatchNumber: true});
	winston.info(`Generated ${subzonesGenerated} total subzones`);
}

export default main;