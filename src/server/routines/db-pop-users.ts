import _ from "lodash";
import winston from "winston";
import inquirer from "inquirer";
import { Generators } from "../data/generators/interfaces";
import { Subroutine } from "./db-pop";
import { ProcessBatchFn, processBatches } from "../util/async";
import { User } from "../models/User";
import { Zone } from "../models/Zone";
import { Location } from "../models/Location";
import { Model } from "../models/Model";
import { Connection, EntitySchema, ObjectType, AdvancedConsoleLogger } from "typeorm";
import { UserPrivacy } from "../models/UserPrivacy";
import { Auth } from "../models/Auth";
import { UserLocation } from "../models/UserLocation";

const BATCH_SIZE = 2500;

async function getOptions(): Promise<{number: number}>{
	const answers: {number?: string | number} = await inquirer.prompt({
		name: "number",
		type: "input",
		message: "Number of users to generate?",
		default: 1000,
	});

	return {
		number: _.toNumber(answers.number),
	}
}

const main: Subroutine = async function(db, generators){
	const options: {number: number} = await getOptions();

	let usersGenerated: number = 0;
	let zonesGenerated: number = 0;
	let locationsGenerated: number = 0;
	
	const nBatches = _.ceil(options.number / BATCH_SIZE);

	const batchFn: ProcessBatchFn = async function(batch){
		let users: User[] = [];

		let rangeStart = (batch * BATCH_SIZE);
		let rangeEnd = _.min([((batch+1) * BATCH_SIZE), options.number]);

		_.each(_.range(rangeStart, rangeEnd), function(){
			const user = generators.user.generate({});
			users.push(user);
			usersGenerated++;
			zonesGenerated += user.zones.length;
			locationsGenerated += user.locations.length;
		});

		await db.transaction(async trans => {
			await trans.save(users);
		});
	}

	await processBatches(nBatches, batchFn, {printBatchNumber: true});
	winston.info(`Generated ${usersGenerated} total users`);
	winston.info(`Generated ${zonesGenerated} total zones`);
	winston.info(`Generated ${locationsGenerated} total locations`);
}

export default main;