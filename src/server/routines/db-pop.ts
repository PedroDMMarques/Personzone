import fs, { exists } from "fs";
import path from "path";
import winston from "winston";
import _ from "lodash";
import { loadEnvironment } from "../util/node";
import { createDatabase } from "../database";
import { createGenerators } from "../data/generators";
import inquirer from "inquirer";
import { GeneratorsConfig, Generators } from "../data/generators/interfaces";

import usersSubroutine from "./db-pop-users";
import subzonesSubroutine from "./db-pop-subzones";
import zonetraitsSubroutine from "./db-pop-zonetraits";
import ratingsSubroutine from "./db-pop-ratings";
import commentsSubroutine from "./db-pop-comments";
import { Connection } from "typeorm";

loadEnvironment([
	"DATABASE_HOST",
	"DATABASE_PORT",
	"DATABASE_USER",
	"DATABASE_PASS",
	"DATABASE_NAME",
]);

async function start(){
	const db = await createDatabase({
		host: process.env.DATABASE_HOST,
		port: _.toNumber(process.env.DATABASE_PORT),
		user: process.env.DATABASE_USER,
		pass: process.env.DATABASE_PASS,
		database: process.env.DATABASE_NAME,
	});

	const configDir: string = path.join(__dirname, "..", "data", "generators", "configs");
	const configNames: string[] = fs.readdirSync(configDir);
	let availableFiles: string[] = [];
	_.each(configNames, function(fName){
		if(path.extname(fName) === ".js")
			availableFiles.push(path.join(configDir, fName));
	});
	
	const answers: {p?: string} = await inquirer.prompt([{
		name: "p",
		message: "Which configuration file to use?",
		type: "list",
		choices: availableFiles
	}]);

	const config: GeneratorsConfig = require(answers.p);
	const generators = createGenerators(config);
	while(true){
		await mainLoop(db, generators);
	}
}

export interface Subroutine {
	(db: Connection, generators: Generators): Promise<any>
}

let subroutine: Subroutine = null;

async function mainLoop(db: Connection, generators: Generators){
	if(!subroutine)
		subroutine = selectSubroutine;

	const temp = subroutine;
	subroutine = null;
	try{
		await temp(db, generators);
	}catch(err){
		winston.error(err);
	}
}

async function selectSubroutine(): Promise<any>{
	const answers: {subroutine? : string} = await inquirer.prompt([{
		name: "subroutine",
		type: "list",
		message: "What type of data to generate?",
		choices: ["users", "subzones", "zonetraits", "ratings", "comments", "exit"],
	}]);

	switch(answers.subroutine){
		case "users": return subroutine = usersSubroutine;
		case "subzones": return subroutine = subzonesSubroutine;
		case "zonetraits": return subroutine = zonetraitsSubroutine;
		case "ratings": return subroutine = ratingsSubroutine;
		case "comments": return subroutine = commentsSubroutine;
		case "exit":
		default:
			process.exit(0);
	}
}

start();