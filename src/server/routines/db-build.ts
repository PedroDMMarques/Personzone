import _ from "lodash";
import winston from "winston";
import inquirer from "inquirer";
import argparse from "argparse";
import { loadEnvironment } from "../util/node";
import { createDatabase } from "../database";
import traits from "../data/traits";
import { Connection } from "typeorm";
import { Trait } from "../models/Trait";

const parser = new argparse.ArgumentParser({
	description: "Build database on the targetd machine (given by loaded environment variables)",
});

parser.addArgument(["-y", "--yes"], {
	help: "Force 'yes' response when prompted to proceed with database creation",
	action: "storeTrue",
	defaultValue: false,
	required: false,
	dest: "force",
});

interface CLIArguments {
	force?: boolean
}

interface inquirerResponse {
	force?: boolean
}

const args: CLIArguments = parser.parseArgs();

loadEnvironment([
	"DATABASE_HOST",
	"DATABASE_PORT",
	"DATABASE_USER",
	"DATABASE_PASS",
	"DATABASE_NAME",
]);

winston.info(`Targeted database: ${process.env.DATABASE_USER}@${process.env.DATABASE_HOST}:${process.env.DATABASE_PORT} - Database: ${process.env.DATABASE_NAME}`);
winston.info("Running this script will wipe out any data in the above database.");

async function start(){
	const answers = <inquirerResponse> await inquirer.prompt([{
		name: "force",
		message: "Are you sure you wish to proceed?",
		type: "confirm",
		default: false,
		when: () => !args["force"],
	}]);

	if(answers.force || args["force"])
		buildDatabase();
}

async function buildDatabase(){
	const db = await createDatabase({
		host: process.env.DATABASE_HOST,
		port: _.toNumber(process.env.DATABASE_PORT),
		user: process.env.DATABASE_USER,
		pass: process.env.DATABASE_PASS,
		database: process.env.DATABASE_NAME,
		sync: true,
	});

	winston.info("Database structure built. Inserting default data...");
	let populatePromises: Promise<any>[] = [
		populateTraits(db).catch(() => winston.error("Error populating database with traits."))
	];

	await Promise.all(populatePromises);
	winston.info("Database built and populated.");
	process.exit(0);
}

async function populateTraits(db: Connection){
	return db.getRepository(Trait)
		.save(_.map(traits, function(trait: string){
			return {name: trait};
		}));
}

start();