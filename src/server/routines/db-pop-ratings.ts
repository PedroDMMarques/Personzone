import _ from "lodash";
import winston from "winston";
import inquirer from "inquirer";
import { Subroutine } from "./db-pop";
import { Connection } from "typeorm";
import { Zonetrait } from "../models/Zonetrait";
import { User } from "../models/User";
import { ProcessBatchFn, processBatches } from "../util/async";
import { Rating } from "../models/Rating";
import { Zone } from "../models/Zone";

const BATCH_SIZE = 1000;

async function getOptions(): Promise<{min: number, max: number}>{
	const answers: {min?: number | string, max?: number | string} = await inquirer.prompt([{
		name: "min",
		type: "input",
		message: "Minimum number of ratings to generate?",
		default: 0,
	},{
		name: "max",
		type: "input",
		message: "Maximum number of ratings to generate?",
		default: 15,
	}]);

	return {
		min: _.toNumber(answers.min),
		max: _.toNumber(answers.max),
	}
}

async function countZonetraits(db: Connection): Promise<number> {
	const obj = await db.getRepository(Zonetrait)
		.createQueryBuilder()
		.select("COUNT(*)", "count")
		.getRawOne();

	return obj.count;
};

async function getUsableUserIds(db: Connection): Promise<number[]> {
	const obj = await db.getRepository(User)
		.find({
			select: ["id"]
		});

	return _.map(obj, o => o.id);
}

const main: Subroutine = async function(db, generators){
	const [options, nZonetraits, userIds]: [{min: number, max: number}, number, number[]] = await Promise.all([
		getOptions(),
		countZonetraits(db),
		getUsableUserIds(db),
	]);

	if(nZonetraits <= 0)
		winston.info("No zonetraits found to generate ratings for");

	let ratingsGenerated: number = 0;
	const nBatches: number = _.ceil(nZonetraits / BATCH_SIZE);
	const batchFn: ProcessBatchFn = async function(batch){
		const zonetraits = await db.getRepository(Zonetrait)
			.find({
				relations: ["ratings", "ratings.user", "zone", "zone.user"],
				take: BATCH_SIZE,
				skip: BATCH_SIZE * batch,
			});

		let ratings: Rating[] = [];
		_.each(zonetraits, function(zonetrait){
			
			let usedUserIds = _.map(zonetrait.ratings, function(rating: Rating){
				return rating.user.id;
			});
			usedUserIds.push((<Zone> zonetrait.zone).user.id);
			let usableIds = _.without(userIds, ...usedUserIds);

			_.each(_.range(_.random(options.min, options.max)), function(){
				let rating = generators.rating.generate({userIds: usableIds});
				if(!rating) return false;
				
				rating.zonetrait = zonetrait;
				usableIds = _.without(usableIds, rating.user.id);
				ratings.push(rating);
				ratingsGenerated++;
				return true;
			});

		});

		await db.transaction(async trans => {
			await trans.save(ratings);
		});

	}

	await processBatches(nBatches, batchFn, {printBatchNumber: true});
	winston.info(`Generated ${ratingsGenerated} total ratings`);
}

export default main;