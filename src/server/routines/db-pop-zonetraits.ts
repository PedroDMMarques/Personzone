import _ from "lodash";
import inquirer from "inquirer";
import winston from "winston";
import { Connection } from "typeorm";
import { Zone } from "../models/Zone";
import { Subroutine } from "./db-pop";
import { ProcessBatchFn, processBatches } from "../util/async";
import { Zonetrait } from "../models/Zonetrait";
import { Trait } from "../models/Trait";

const BATCH_SIZE = 1000;

async function getOptions(): Promise<{min: number, max: number}>{
	const answers: {min?: number | string, max?: number | string} = await inquirer.prompt([{
		name: "min",
		type: "input",
		message: "Minimum number of zonetraits to generate?",
		default: 0,
	},{
		name: "max",
		type: "input",
		message: "Maximum number of zonetraits to generate?",
		default: 15,
	}]);

	return {
		min: _.toNumber(answers.min),
		max: _.toNumber(answers.max),
	};
}

async function countZones(db: Connection): Promise<number> {
	const obj = await db.getRepository(Zone)
		.createQueryBuilder()
		.select("COUNT(*)", "count")
		.getRawOne();

	return obj.count;
};

async function getUsableTraitIds(db: Connection): Promise<number[]> {
	const obj = await db.getRepository(Trait)
		.find({
			select: ["id"]
		});

	return _.map(obj, o => o.id);
}

const main: Subroutine = async function(db, generators){
	const [options, nZones, traitIds]: [{min: number, max: number}, number, number[]] = await Promise.all([
		getOptions(),
		countZones(db),
		getUsableTraitIds(db),
	]);

	if(nZones <= 0)
		winston.info("No zones found to generate zonetraits for");

	let zonetraitsGenerated: number = 0;
	const nBatches: number = _.ceil(nZones / BATCH_SIZE);
	const batchFn: ProcessBatchFn = async function(batch){
		const zones = await db.getRepository(Zone)
			.find({
				select: ["id"],
				relations: ["zonetraits", "zonetraits.trait"],
				take: BATCH_SIZE,
				skip: BATCH_SIZE * batch,
			});

		let zonetraits: Zonetrait[] = [];
		_.each(zones, function(zone){
			
			let usedTraitIds = _.map(zone.zonetraits, function(zonetrait: Zonetrait){
				return zonetrait.trait.id;
			});
			let usableTraits = _.without(traitIds, ...usedTraitIds);

			_.each(_.range(_.random(options.min, options.max)), function(){
				let zonetrait = generators.zonetrait.generate({traitIds: usableTraits})
				if(!zonetrait) return false;
				
				zonetrait.zone = zone;
				usableTraits = _.without(usableTraits, zonetrait.trait.id);
				zonetraits.push(zonetrait);
				zonetraitsGenerated++;
				return true;
			});

		});

		await db.transaction(async trans => {
			await trans.save(zonetraits);
		});

	}

	await processBatches(nBatches, batchFn, {printBatchNumber: true});
	winston.info(`Generated ${zonetraitsGenerated} total subzones`);
}

export default main;