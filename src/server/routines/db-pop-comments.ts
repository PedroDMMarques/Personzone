import _ from "lodash";
import winston from "winston";
import inquirer from "inquirer";
import { Subroutine } from "./db-pop";
import { Connection } from "typeorm";
import { Zonetrait } from "../models/Zonetrait";
import { User } from "../models/User";
import { ProcessBatchFn, processBatches } from "../util/async";
import { Rating } from "../models/Rating";
import { Zone } from "../models/Zone";
import { Subzone } from "../models/Subzone";
import { Comment } from "../models/Comment";

const BATCH_SIZE = 1000;

async function getOptions(): Promise<{min: number, max: number}>{
	const answers: {min?: number | string, max?: number | string} = await inquirer.prompt([{
		name: "min",
		type: "input",
		message: "Minimum number of comments to generate?",
		default: 0,
	},{
		name: "max",
		type: "input",
		message: "Maximum number of comments to generate?",
		default: 15,
	}]);

	return {
		min: _.toNumber(answers.min),
		max: _.toNumber(answers.max),
	}
}

async function countSubzones(db: Connection): Promise<number> {
	const obj = await db.getRepository(Subzone)
		.createQueryBuilder()
		.select("COUNT(*)", "count")
		.getRawOne();

	return obj.count;
};

async function getUsableUserIds(db: Connection): Promise<number[]> {
	const obj = await db.getRepository(User)
		.find({
			select: ["id"]
		});

	return _.map(obj, o => o.id);
}

const main: Subroutine = async function(db, generators){
	const [options, nSubzones, userIds]: [{min: number, max: number}, number, number[]] = await Promise.all([
		getOptions(),
		countSubzones(db),
		getUsableUserIds(db),
	]);

	if(nSubzones <= 0)
		winston.info("No subzones found to generate ratings for");

	let commentsGenerated: number = 0;
	const nBatches: number = _.ceil(nSubzones / BATCH_SIZE);
	const batchFn: ProcessBatchFn = async function(batch){
		const subzones = await db.getRepository(Subzone)
			.find({
				relations: ["comments", "comments.user", "zone", "zone.user"],
				take: BATCH_SIZE,
				skip: BATCH_SIZE * batch,
			});

		let comments: Comment[] = [];
		_.each(subzones, function(subzone){
			
			let usedUserIds = _.map(subzone.comments, function(comment: Comment){
				return comment.user.id;
			});
			usedUserIds.push((<Zone> subzone.zone).user.id);
			let usableIds = _.without(userIds, ...usedUserIds);

			_.each(_.range(_.random(options.min, options.max)), function(){
				let comment = generators.comment.generate({userIds: usableIds});
				if(!comment) return false;
				
				comment.subzone = subzone;
				usableIds = _.without(usableIds, comment.user.id);
				comments.push(comment);
				commentsGenerated++;
				return true;
			});

		});

		await db.transaction(async trans => {
			await trans.save(comments);
		});

	}

	await processBatches(nBatches, batchFn, {printBatchNumber: true});
	winston.info(`Generated ${commentsGenerated} total comments`);
}

export default main;