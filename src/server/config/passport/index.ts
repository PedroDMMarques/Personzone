import _ from "lodash";
import { createDatabase } from "../../database";
import { configLocalStrategy } from "./LocalStrategy";

import passport from "passport";
import { User } from "../../models/User";
import { Connection } from "typeorm";
import { getUser } from "../../util/dbqueries/user";

export async function configPassport(db: Connection){
	passport.serializeUser(function(user: User, done){
		done(null, user.id);
	});
	
	passport.deserializeUser(async function(id: number, done){
		const user = await getUser(db, {id});
		
		if(user){
			done(null, user);
		}else{
			done(new Error(`Id ${id} does not translate to a user in the database`));
		}
	});

	configLocalStrategy(db);
}