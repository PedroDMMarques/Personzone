import _ from "lodash";
import { createDatabase } from "../../database";

import passport from "passport";
import { Strategy } from "passport-local";
import { User } from "../../models/User";
import { Auth } from "../../models/Auth";
import { Connection } from "typeorm";
import { getUser } from "../../util/dbqueries/user";

export async function configLocalStrategy(db: Connection){
	passport.use(new Strategy({
		usernameField: "email",
		passwordField: "password",
	}, async function(email, password, done){
		try{
			const user = await getUser(db, {email});
			
			if(!user) return done(null, false);
			if(!(<Auth> user.auth).validatePassword(password)) return done(null, false);
			return done(null, user);
		}catch(err){
			done(err);
		}
	}));
}