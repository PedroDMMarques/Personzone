import { Router, RequestHandler } from "express";

export function createAuthRoutes(): Router {
	const router = Router();

	router.get("/logged-in-user", loggedInUser());

	return router;
}

function loggedInUser(): RequestHandler[] {
	return [
		function(req, res, next){
			res.send(req.user);
		}
	]
}