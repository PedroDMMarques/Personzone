import { Router } from "express";
import { RequestHandler } from "express-serve-static-core";

export function createI18NRoutes(): Router {
	const router = Router();
	
	router.get("/current-locale", currentLocale());
	router.get("/current-locale-phrases", currentLocalePhrases());

	return router;
}

function currentLocale(): RequestHandler {
	return function(req, res, next){
		res.send(req.localizer.locale);
	}
}

function currentLocalePhrases(): RequestHandler {
	return function(req, res, next){
		res.send(req.localizer.loadLocale().phrases);
	}
}