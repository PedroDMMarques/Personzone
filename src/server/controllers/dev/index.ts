import { Router, RequestHandler } from "express";

import { createAuthRoutes } from "./auth";
import { createI18NRoutes } from "./i18n";
import { validateRequest, asyncMiddleware } from "../../middleware/util";
import { getEmail, getUserPrimaryEmail } from "../../util/dbqueries/email";
import { getUser } from "../../util/dbqueries/user";

export function createDevRoutes(): Router {
	const router = Router();

	router.use("/i18n", createI18NRoutes());
	router.use("/auth", createAuthRoutes());
	router.get("/testing", testing());

	return router;
}

function testing(): RequestHandler[] {
	return [
		asyncMiddleware(async (req, res, next) => {
			const user = await getUser(req.db, { id: 1 });
			const email = await getUserPrimaryEmail(req.db, {user});

			res.send(email);
		})
	]
}