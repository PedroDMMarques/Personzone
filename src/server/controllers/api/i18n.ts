import { Router, RequestHandler } from "express";
import { check } from "express-validator/check";
import { validateRequest } from "../../middleware/util";
import { matchedData } from "express-validator/filter";
import { LocaleNotFoundError } from "../../errors/apiErrors/LocaleNotFoundError";

export function createI18NRoutes(): Router {
	const router = Router();

	router.post("/", setLocale());
	router.delete("/", clearLocale());

	return router;
}

function setLocale(): RequestHandler[] {
	return [
		check("locale").exists().isString(),
		validateRequest(),
		function(req, res, next){
			const params = matchedData(req);

			try {
				req.localizer.setLocale(params.locale);
				res.send(200);
			} catch(e) {
				throw new LocaleNotFoundError(params.locale);
			}
		}
	];
}

function clearLocale(): RequestHandler {
	return function(req, res, next){
		req.localizer.clearLocale();
		res.send(200);
	};
}