import { Router, RequestHandler } from "express";
import passport from "passport";
import { updateLastLogin } from "../../util/dbqueries/auth";
import { asyncMiddleware } from "../../middleware/util";

export function createAuthRoutes(): Router {
	const router = Router();

	router.post("/login", login());
	router.get("/logout", logout());

	return router;
}

function login(): RequestHandler[] {
	return [
		passport.authenticate("local"),
		asyncMiddleware(async (req, res, next) => {
			// By this point authentication is successful
			const user = await updateLastLogin(req.db, {getUser: { id: req.user.id }});
			res.send(user);	
		})
	]
}

function logout(): RequestHandler[] {
	return [
		function(req, res, next){
			req.logout();
			res.sendStatus(200);
		}
	]
};