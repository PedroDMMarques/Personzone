import { Router } from "express";
import { createUserRoutes } from "./users";
import { createAuthRoutes } from "./auth";
import { createI18NRoutes } from "./i18n";

export function createApiRoutes(){
	const router = Router();

	router.use("/users", createUserRoutes());
	router.use("/auth", createAuthRoutes());
	router.use("/i18n", createI18NRoutes());

	return router;
}

/**
 * @apiDefine EmailIsPrimaryError
 * @apiError EmailIsPrimaryError The action cannot be completed because the email is set as primary
 * @apiErrorExample {json} EmailIsPrimaryError
 * 		HTTP/1.1 400 Bad request
 * 		{
 * 			error: "EmailIsPrimaryError",
 *			message: "Email 'a@b.c' is set as primary"
 * 		}
 */

/**
 * @apiDefine EmailNotFoundError
 * @apiError EmailNotFoundError The email could not be found
 * @apiErrorExample {json} EmailNotFoundError
 * 		HTTP/1.1 404 Not found
 * 		{
 * 			error: "EmailNotFoundError",
 *			message: "Email 'a@b.c' was not found"
 * 		}
 */

/**
 * @apiDefine UnauthorizedError
 * @apiError UnauthorizedError You are not authorized to make that request
 * @apiErrorExample {json} UnauthorizedError
 * 		HTTP/1.1 401 Unauthorized
 * 		{
 * 			error: "UnauthorizedError",
 *			message: "You are not authorized to make that request"
 * 		}
 */

/**
 * @apiDefine UserNotFoundError
 * @apiError UserNotFoundError The user could not be found
 * @apiErrorExample {json} UserNotFoundError
 * 		HTTP/1.1 404 Not found
 * 		{
 * 			error: "UserNotFoundError",
 * 			message: "User 'pedro123' was not found"
 * 		}
 */

/**
 * @apiDefine LocaleNotFoundError
 * @apiError LocaleNotFoundError The locale count not be found
 * @apiErrorExample {json} LocaleNotFoundError
 * 		HTTP/1.1 404 Not found
 * 		{
 * 			error: "LocaleNotFoundError",
 *			message: "Locale 'de' could not be found"
 * 		}
 */