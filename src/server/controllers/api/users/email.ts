import { Router, RequestHandler } from "express";
import { isPurl, isEmail } from "../../../util/validation";
import { validateRequest, asyncMiddleware } from "../../../middleware/util";
import { check } from "express-validator/check";
import { matchedData } from "express-validator/filter";
import { UnauthorizedError } from "../../../errors/apiErrors/UnauthorizedError";

import {
	createEmail as dbCreateEmail,
	getEmail as dbGetEmail,
	verifyEmail as dbVerifyEmail,
	setEmailPrimary as dbSetEmailPrimary,
	deleteEmail as dbDeleteEmail,
} from "../../../util/dbqueries/email";

import { POSTGRES_ERROR_CODES } from "../../../util/postgresErrorCodes";
import { EmailAlreadyExistsError } from "../../../errors/apiErrors/EmailAlreadyExistsError";
import { UnknownError } from "../../../errors/apiErrors/UnknownError";
import { EmailNotFoundError } from "../../../errors/apiErrors/EmailNotFoundError";
import { InvalidEmailVerificationHashError } from "../../../errors/apiErrors/InvalidEmailVerificationHashError";
import { User } from "../../../models/User";
import { EmailIsPrimaryError } from "../../../errors/apiErrors/EmailIsPrimaryError";

export function createUserEmailRoutes(): Router {
	const router = Router();

	router.post("/:qpurl/email", createEmail());

	router.get("/:qpurl/email/:qemail/verify", verifyEmail());

	// This is set has post so that people can't be tricked by clicking on a simple URL
	// For example, say an attacker has compromised a secondary email of a user, but needs that email to be primary to login
	// Another example is if the user clicking in a link to this doesn't realize it, and then can't login because they try to use the previous email
	router.post("/:qpurl/email/:qemail/primary", setPrimary());

	router.delete("/:qpurl/email/:qemail", deleteEmail());

	return router;
}

/**
 * @api {delete} /users/:qpurl/email/:email Delete email
 * @apiName Delete email
 * @apiGroup Emails
 * 
 * @apiParam {string} qpurl The personal url of the user to delete the email of
 * @apiParam {string} qemail The "name" of the email to delete
 * 
 * @apiExample {json} Response
 * 		HTTP/1.1 200 OK
 * 
 * @apiUse EmailIsPrimaryError
 * @apiUse EmailNotFoundError
 * @apiUse UnauthorizedError
 */
function deleteEmail(): RequestHandler[] {
	return [
		check("qpurl").exists().custom(isPurl),
		check("qemail").exists().custom(isEmail),
		validateRequest(),
		asyncMiddleware(async (req, res, next) => {
			const params = matchedData(req);

			if(!req.user)
				throw new UnauthorizedError();
			
			if(req.user.purl !== params.qpurl)
				throw new UnauthorizedError();

			const email = await dbGetEmail(req.db, {email: params.qemail});
			if(!email)
				throw new EmailNotFoundError(params.qemail);

			if(email.primary)
				throw new EmailIsPrimaryError(email.email);

			if((<User> email.user).purl !== params.qpurl)
				throw new UnauthorizedError();
			
			await dbDeleteEmail(req.db, {email});
			res.send(200);
		})
	]
}

function setPrimary(): RequestHandler[] {
	return [
		check("qpurl").exists().custom(isPurl),
		check("qemail").exists().custom(isEmail),
		validateRequest(),
		asyncMiddleware(async (req, res, next) => {
			const params = matchedData(req);

			if(!req.user)
				throw new UnauthorizedError();

			if(req.user.purl !== params.qpurl)
				throw new UnauthorizedError();

			const email = await dbGetEmail(req.db, {email: params.qemail});
			if(!email)
				throw new EmailNotFoundError(params.qemail);
					
			if(email.user.id !== req.user.id)
				throw new UnauthorizedError();
			
			if(await dbSetEmailPrimary(req.db, {email}))
				res.send(200);
			else
				throw new UnknownError("Could not set email primary");
		})
	];
}

function verifyEmail(): RequestHandler[] {
	return [
		check("qpurl").exists().custom(isPurl),
		check("qemail").exists().custom(isEmail),
		check("hash").exists().isString(),
		validateRequest(),
		asyncMiddleware(async (req, res, next) => {
			const params = matchedData(req);

			const email = await dbGetEmail(req.db, {email: params.qemail});
			if(!email)
				throw new EmailNotFoundError(params.qemail);

			if(params.hash !== email.verificationHash)
				throw new InvalidEmailVerificationHashError(params.hash);

			if(await dbVerifyEmail(req.db, {email, hash: params.hash})){
				res.send(200);
			}else{
				throw new UnknownError("Cound not verify email");
			}
		})
	]
}

function createEmail(): RequestHandler[] {
	return [
		check("qpurl").exists().custom(isPurl),
		check("email").exists().custom(isEmail),
		validateRequest(),
		asyncMiddleware(async (req, res, next) => {
			const params = matchedData(req);

			if(!req.user)
				throw new UnauthorizedError();
			if(req.user.purl !== params.qpurl)
				throw new UnauthorizedError();

			try{
				const email = await dbCreateEmail(req.db, {
					getUser: { purl: params.qpurl },
					email: params.email,
				});

				const template = new req.mailer.templates.VerifyEmail({
					to: email.email,
					purl: params.qpurl,
					hash: email.verificationHash,
				});
				req.mailer.sendTemplate(template);

				res.status(201).send(email);

			}catch(err){
				if(err.code === POSTGRES_ERROR_CODES.UNIQUE_CONSTRAINT){
					if(err.detail.indexOf("email") !== -1){
						throw new EmailAlreadyExistsError(params.email);
					}
				}

				throw new UnknownError("Error creating email", err);
			}
		})
	]
}