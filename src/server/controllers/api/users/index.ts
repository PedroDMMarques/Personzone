import { Router, RequestHandler } from "express";

import { check } from "express-validator/check";
import { matchedData } from "express-validator/filter";

import { asyncMiddleware, validateRequest } from "../../../middleware/util";
import { User } from "../../../models/User";

import { POSTGRES_ERROR_CODES } from "../../../util/postgresErrorCodes";
import {
	createUser as dbCreateUser,
	updateUser as dbUpdateUser,
	getUser as dbGetUser,
} from "../../../util/dbqueries/user";
import {
	changePassword as dbChangePassword,
} from "../../../util/dbqueries/auth";

import { EmailAlreadyExistsError } from "../../../errors/apiErrors/EmailAlreadyExistsError";
import { UnknownError } from "../../../errors/apiErrors/UnknownError";
import { UnauthorizedError } from "../../../errors/apiErrors/UnauthorizedError";
import { isEmail, isPurl, isDate, isDateOrNull, toDateOrNull, isBooleanOrNull, toBooleanOrNull } from "../../../util/validation";
import { PRIVACY_SETTINGS, GENDERS, BLOOD_TYPES, REL_STATUSES } from "../../../util/enums";
import { UserEmail } from "../../../models/UserEmail";
import { createUserEmailRoutes } from "./email";
import { UserNotFoundError } from "../../../errors/apiErrors/UserNotFoundError";

export function createUserRoutes(): Router {
	const router = Router();
	
	router.use(createUserEmailRoutes());
	
	router.post("/", createUser());
	router.post("/:qpurl", updateUser());
	router.post("/:qpurl/password", changePassword());

	router.get("/:qpurl", getUser());
	router.get("/", getUsers());

	return router;
}

/**
 * @api {post} /user/:qpurl/password Change password
 * @apiName Change password
 * @apiGroup Users
 * 
 * @apiParam {string} qpurl The personal url of the user to change the password of
 * @apiParam {string} password The new password
 * @apiParam {string} oldPassword The previous password
 * 
 * @apiExample {json} Response
 * 		HTTP/1.1 200 OK
 * 
 * @apiUse UnauthorizedError
 * @apiUse UserNotFoundError
 */
function changePassword(): RequestHandler[] {
	return [
		check("qpurl").exists().custom(isPurl),
		check("password").exists().isString(),
		check("oldPassword").exists().isString(),
		validateRequest(),
		asyncMiddleware(async (req, res, next) => {
			const params = matchedData(req);
			if(!req.user)
				throw new UnauthorizedError();

			if(req.user.purl !== params.qpurl)
				throw new UnauthorizedError();

			try{
				const user = await dbGetUser(req.db, {purl: params.qpurl});
				if(!user)
					throw new UserNotFoundError(params.purl);
				
				await dbChangePassword(req.db, {user, newPass: params.password, oldPass: params.oldPassword});
				res.send(200);
			}catch(e){
				if(e instanceof ReferenceError)
					throw new UnauthorizedError();
				else
					throw e;
			}
		})
	]
}


function getUser(): RequestHandler[] {
	return [
		check("qpurl").exists().custom(isPurl),
		validateRequest(),
		asyncMiddleware(async (req, res, next) => {
			const params = matchedData(req);

			//@todo: What the fuct is going on here...?
			try{

				let searcher: string = null;
				if(req.user)
					searcher = (<User> req.user).purl;
	
				let user = await dbGetUser(req.db, {purl: params.qpurl});
				if(!user)
					return res.status(404).send();
				
				if(req.user){
					if(req.user.purl === params.qpurl)
						user = User.getPrivateProfile(user);
					else
						user = User.getPublicProfile(user);
				}else{
					user = User.getPublicProfile(user);
				}
	
				return res.send(user);
			}catch(e){
				console.log(e);
				return null;
			}
		})
	];
}

function getUsers(): RequestHandler[] {
	return [

	];
}

function updateUser(): RequestHandler[] {
	return [
		check("qpurl").exists().custom(isPurl),
		check("name").optional().isString(),
		check("purl").optional().custom(isPurl),
		check("gender").optional().isIn(Object.keys(GENDERS).concat(null)),
		check("dob").optional().custom(isDateOrNull),
		check("bloodType").optional().isIn(Object.keys(BLOOD_TYPES)),
		check("drivingLicense").optional().custom(isBooleanOrNull),
		check("seekingWork").optional().custom(isBooleanOrNull),
		check("relStatus").optional().isIn(Object.keys(REL_STATUSES).concat(null)),
		check("defWord").optional().isString(),
		check("joinDatePrivacy").optional().isIn(Object.keys(PRIVACY_SETTINGS)),
		check("genderPrivacy").optional().isIn(Object.keys(PRIVACY_SETTINGS)),
		check("dobPrivacy").optional().isIn(Object.keys(PRIVACY_SETTINGS)),
		check("bloodTypePrivacy").optional().isIn(Object.keys(PRIVACY_SETTINGS)),
		check("drivingLicensePrivacy").optional().isIn(Object.keys(PRIVACY_SETTINGS)),
		check("seekingWorkPrivacy").optional().isIn(Object.keys(PRIVACY_SETTINGS)),
		check("relStatusPrivacy").optional().isIn(Object.keys(PRIVACY_SETTINGS)),
		check("defWordPrivacy").optional().isIn(Object.keys(PRIVACY_SETTINGS)),
		validateRequest(),
		asyncMiddleware(async (req, res, next) => {
			const params = matchedData(req);

			if(params.drivingLicense)
				params.drivingLicense = toBooleanOrNull(params.drivingLicense);
			if(params.seekingWork)
				params.seekingWork = toBooleanOrNull(params.seekingWork);

			if(!req.user)
				throw new UnauthorizedError();
		
			if(req.user.purl !== params.qpurl)
				throw new UnauthorizedError();

			try{
				const user = await dbUpdateUser(req.db, {
					getUser: {purl: params.qpurl},
					...params
				});

				return res.send(user);
			}catch(err){
				throw new UnknownError("Error updating user", err);
			}
		})
	]
}

function createUser(): RequestHandler[] {
	return [
		check("name").exists(),
		check("email").exists().custom(isEmail),
		check("password").exists(),
		validateRequest(),
		asyncMiddleware(async (req, res, next) => {
			const params = matchedData(req);
			try{
				const user = await dbCreateUser(req.db, {
					name: params.name,
					email: params.email,
					password: params.password,
				});

				const verificationHash = (<UserEmail> user.emails[0]).verificationHash;
				const purl = user.purl;
				const email = (<UserEmail> user.emails[0]).email;

				const template = new req.mailer.templates.VerifyEmail({
					to: email,
					purl: purl,
					hash: verificationHash,
				});

				req.login(user, function(err){
					res.status(201).send(user);
				});
				
				req.mailer.sendTemplate(template);
				
			}catch(err){
				if(err.code === POSTGRES_ERROR_CODES.UNIQUE_CONSTRAINT){
					if(err.detail.indexOf("email") !== -1){
						throw new EmailAlreadyExistsError(params.email);
					}
				}

				throw new UnknownError("Error creating user", err);
			}
		})
	];
}