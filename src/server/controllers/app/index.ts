import { Router } from "express";

export function createAppRoutes(): Router {
	const router = Router();

	router.get("/", function(req, res, next){
		res.send("Hello there matey!");
	});

	return router;
}