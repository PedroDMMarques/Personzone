import { RequestHandler, Request, Response, NextFunction } from "express";
import { validationResult } from "express-validator/check";
import { RequestValidationError } from "../../errors/apiErrors/RequestValidationError";

/**
 * Enable middleware functions to use async/await
 * Use this in router.get(..., asyncMiddleware(async (req, res, next) => {...}))
 * If the function fails (.catch) the NextFunction is called
 * @returns {RequestHandler} middleware functions to be used in routes
 */
export function asyncMiddleware(fn: RequestHandler): RequestHandler {
	return function(req, res, next){
		return Promise.resolve(fn(req, res, next)).catch(next);
	}
}

/**
 * Validate the request parameters specified with express-validator
 * If there is an error throws a RequestValidationError automatically
 * After this use 'matchedData' as usual to get the parameters. Doing so makes sure not to pollute the Request object even further
 * @returns {RequestHandler}
 */
export function validateRequest(): RequestHandler {
	return function(req: Request, res: Response, next: NextFunction){
		const e = validationResult(req);
		if(!e.isEmpty())
			throw new RequestValidationError(e);
	
		next();
	}
}