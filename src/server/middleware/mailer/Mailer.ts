import _ from "lodash";
import { Config } from "./index";
import { createTransport, SendMailOptions } from "nodemailer";
import { templates, Template } from "./templates";

//@TODO: Can this hack be fixed? nodemailer doesn't export Mail, those idiots
//const h = createTransport();
//type Mail = typeof h;

interface Mail {
	sendMail: any
	verify: any
}

export class Mailer {

	private mailer: Mail;
	templates = templates;

	constructor(config: Config){
		this.mailer = createTransport({
			host: config.host,
			port: config.port,
			auth: {
				user: config.user,
				pass: config.pass,
			}
		}, config.defaults);
	}

	sendTemplate(template: Template): Promise<any> {
		return this.mailer.sendMail(template.getData());
	}

	verify(){ return this.mailer.verify() };
}