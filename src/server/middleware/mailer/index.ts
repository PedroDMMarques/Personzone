import _ from "lodash";
import { RequestHandler } from "express";
import winston from "winston";
import nodemailer from "nodemailer";
import { Mailer } from "./Mailer";
import { MiddlewareConfig } from "../middleware";

export interface Config extends MiddlewareConfig {
	/**
	 * The port to connect to the SMTP server on
	 */
	port: number

	/**
	 * The host of the SMTP server
	 */
	host: string

	/**
	 * The username to use when connection to the SMTP server
	 */
	user: string

	/**
	 * The password for accessing the SMTP server
	 */
	pass: string

	/**
	 * Whether to print out service status upon initialization. Defaults to true
	 */
	printStatus?: boolean,

	/**
	 * The defaults to use for every message
	 */
	defaults: TransportDefaults
}

interface TransportDefaults {
	/**
	 * The name of the sender (`Personzone personzone@personzone.com`)
	 */
	from: string
}

export async function createMailer(config: Config): Promise<RequestHandler> {
	if(_.isUndefined(config.printStatus))
		config.printStatus = true;
	
	if(config.printStatus)
		winston.info(`Connecting to mailer service ${config.user}@${config.host}:${config.port}`);
	
	const mailer = new Mailer(config);
	try{
		await mailer.verify()
		if(config.printStatus)
			winston.info("Mailer system connected");
	}catch(err){
		winston.error(err);
	}

	return function(req, res, next){
		req.mailer = mailer;
		next();
	}
}