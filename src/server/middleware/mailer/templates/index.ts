import { SendMailOptions } from "nodemailer";

export interface RenderedTemplate {
	subject: string
	text: string
}

export interface TemplateParams {
	/**
	 * The email address to send the template to
	 */
	to: string
}

export abstract class Template {
	data: SendMailOptions = {}

	constructor(params: TemplateParams){
		this.data.to = params.to;
	}

	getData(){
		return this.data;
	}
}

import VerifyEmail from "./VerifyEmail";
export const templates = {
	VerifyEmail
}