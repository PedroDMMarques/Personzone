import _ from "lodash";
import { TemplateParams, Template } from "./index";

interface Params extends TemplateParams {
	/**
	 * The personal url of the user
	 */
	purl: string

	/**
	 * The email verification hash
	 */
	hash: string
}


export default class VerifyEmail extends Template {
	constructor(params: Params){
		super(params);
		this.data.subject = "Email Verification";
		this.data.text = `Thank you for registering with personzone. Click the link to verify your email <http://localhost:3000/api/users/${params.purl}/verify-email/${params.hash}>
			.\n\nDISCLAIMER: Personzone is an upcoming web application currently in the first stages of being built.
			If you have received this email and have no idea of what personzone is then please disregard it.
			This stupid developer humbly apoloziges for being a nuisance.`;
	}
}