import { MiddlewareConfig } from "../middleware";
import { ErrorRequestHandler } from "express";
import { ApiError } from "../../errors/apiErrors/ApiError";
import { NODE_ENV } from "../../util/enums";
import { UnknownError } from "../../errors/apiErrors/UnknownError";

interface Config extends MiddlewareConfig {

}

export function createErrorHandler(config: Config): ErrorRequestHandler {
	return function(err, req, res, next){
		let status = err.status || 500;
		let response = {};
		
		if(!(err instanceof ApiError))
			err = new UnknownError("", err);

		status = err.getResponseStatus();
		response = process.env.NODE_ENV === NODE_ENV.DEVELOPMENT ?
			err.getDevResponseObject() : 
			err.getResponseObject();

		// Send only if it hasn't been sent already in some other part of the chain
		if(!res.headersSent)
			res.status(status).send(response);
		
		next();
	}
}