import { RequestHandler } from "express";

import { MiddlewareConfig } from "../middleware";
import { Connection } from "typeorm";

interface Config extends MiddlewareConfig {
	connection: Connection
}

export function createExpressDB(config: Config): RequestHandler {
	return function(req, res, next){
		req.db = config.connection;
		next();
	}
}