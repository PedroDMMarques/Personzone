import fs from "fs";

import _ from "lodash";

import { RequestHandler } from "express";
import { MiddlewareConfig } from "../middleware";

const auth = require("basic-auth");

interface BasicAuthConfig extends MiddlewareConfig {
	/**
	 * The realm to send on WWW-Authenticate 401 response
	 */
	realm: string,

	/**
	 * The path to the .basicauth file containing all of the credentials
	 */
	basicAuthFile: string,
}

export function createBasicAuth(config: BasicAuthConfig): RequestHandler {
	const credentials: {
		[name: string]: { password: string }
	} = {};

	_.each(fs.readFileSync(config.basicAuthFile, "utf8").split("\n"), function(line){
		const split = line.split(",");
		credentials[split[0]] = {password: split[1]};
	});

	return function(req, res, next){
		const user = auth(req);
		if(!user || !credentials[user.name] || credentials[user.name].password !== user.pass){
			res.set("WWW-Authenticate", `Basic realm=${config.realm}`);
			return res.status(401).send();
		}

		return next();
	}
}