import fs from "fs";
import path from "path";

import _ from "lodash";
import acceptLanguageParser, { Language } from "accept-language-parser";

import { Request, Response } from "express";

interface ConstructorParams {
	/**
	 * The directory containing the locales
	 */
	localeDir: string
	/**
	 * The default locale to use
	 */
	defaultLocale: string
	/**
	 * An array of available locales
	 */
	availableLocales: string[]
	/**
	 * The name to use for the locale cookie
	 */
	localeCookie: string
	/**
	 * The client request to work with
	 */
	req: Request
	/**
	 * The client response to work with
	 */
	res: Response
}

export class Localizer {
	readonly localeDir: string
	readonly defaultLocale: string
	readonly availableLocales: string[]
	private readonly localeCookie: string
	private readonly req: Request
	private readonly res: Response

	/**
	 * The locale read from the request
	 */
	readonly locale: string

	constructor(params: ConstructorParams){
		this.localeDir = params.localeDir;
		this.defaultLocale = params.defaultLocale;
		this.availableLocales = params.availableLocales;
		this.localeCookie = params.localeCookie;
		this.req = params.req;
		this.res = params.res;

		if(!this.localeExists(this.defaultLocale))
			throw new ReferenceError(`Default locale ${this.defaultLocale} does not exist`);

		this.locale = this.getRequestLocale();
	}

	/**
	 * Set the locale (cookie)
	 * @param {string} locale The locale to set
	 * @throws {ReferenceError} if the locale does not exist
	 */
	setLocale(locale: string){
		if(!this.localeExists(locale))
			throw new ReferenceError(`Locale ${locale} does not exist`);

		this.res.cookie(this.localeCookie, locale);
	}

	/**
	 * Clear the selected locale (cookie)
	 */
	clearLocale(){
		this.res.clearCookie(this.localeCookie);
	}

	/**
	 * Load the locale object ({code, phrases}), used to initialize polyglot
	 * @returns {object} the locale object used to initialize polyglot
	 */
	loadLocale(): {code: string, phrases: {[name: string]: string}} {
		const localeDir = path.join(this.localeDir, this.locale);
		let phrases: {[name: string]: string} = {};

		_.forEach(fs.readdirSync(localeDir), function(fname){
			phrases[fname.replace(/\.[^/.]+$/, "")] = require(path.join(localeDir, fname));
		});

		return { code: this.locale, phrases };
	}

	/**
	 * Get the locale to use for the request
	 * First checks locale cookie. If locale cookie doesn't exist checks accept-language http header.
	 * If neither of those exist return the default locale.
	 * @returns {string} The locale used for the request
	 */
	private getRequestLocale(){
		if(this.req.cookies[this.localeCookie])
			return this.req.cookies[this.localeCookie];

		let locale: string = null;
		let clientAccept = acceptLanguageParser.parse(<string> this.req.headers["accept-language"]);
		_.forEach(clientAccept, function(o: Language){
			if(this.localeExists(o.code))
				locale = o.code;
			
			// Stop the loop prematurely if we found a valid locale
			return locale === null;
		}.bind(this));

		return locale ? locale : this.defaultLocale;
	}

	/**
	 * Check if a locale exists
	 * @param {string} locale The locale to check
	 * @returns {boolean} true if the locale exists, false otherwise
	 */
	localeExists(locale: string){
		return this.availableLocales.indexOf(locale) !== -1;
	}

	/**
	 * Get all of the available locales present in the provided directory
	 * @param {string} dir The directory to search for locales in
	 * @returns {string[]} an array of all the available locales
	 */
	static searchAvailableLocales(dir: string){
		return fs.readdirSync(dir).filter(function(fname){
			return fs.lstatSync(path.join(dir, fname)).isDirectory();
		});
	}
}