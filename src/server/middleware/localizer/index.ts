import { Localizer } from "./Localizer";

import { MiddlewareConfig } from "../middleware";
import { RequestHandler } from "express";

interface Config extends MiddlewareConfig {
	/**
	 * The directory containing all of the available locales
	 */
	localeDir: string
	defaultLocale: string
	localeCookie?: string
}

export function createLocalizer(config: Config): RequestHandler {
	const availableLocales = Localizer.searchAvailableLocales(config.localeDir);
	const localeCookie = config.localeCookie || "locale";

	return function(req, res, next){
		req.localizer = new Localizer({
			availableLocales, localeCookie, defaultLocale: config.defaultLocale,
			req, res, localeDir: config.localeDir
		});

		next();
	}
}