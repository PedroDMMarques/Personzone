const path = require("path");
const webpack = require("webpack");
const BundleAnalyzerPlugin = require("webpack-bundle-analyzer").BundleAnalyzerPlugin;
const UglifyJsPlugin = require("uglifyjs-webpack-plugin");

require("dotenv").config({
	path: path.join(__dirname, "..", "..", ".env")
});

function isExternal(module){
	const context = module.context;

	if(typeof context != "string")
		return false;

	return context.indexOf("node_modules") !== -1;
}

module.exports = {

	mode: process.env.NODE_ENV,

	// Entry point for webpack
	// Add multiple lines for each SPA
	entry: {
		"app": "./js/app/index.ts",
	},

	// The output of the compilation
	output: {
		path: path.join(__dirname, "..", "..", "public", "script"),
		filename: "[name].min.js",
	},

	// The modules to use for compiling
	module: {
		rules: [{
			test: /\.ts?$/,
			use: "awesome-typescript-loader",
		}]
	},

	optimization: {
		splitChunks: {
			cacheGroups: {
				vendor: {
					test: /[\\/]node_modules[\\/]/,
					
					name: "vendors",
					
					enforce: true,
					chunks: "all",
				}
			}
		}
	},

	plugins: [
		new UglifyJsPlugin({
			include: /\.min\.js$/,
		}),

		new BundleAnalyzerPlugin({
			analyzerMode: "static",
			reportFilename: path.join(__dirname, "..", "..", "docs", "bundles_report.html"),
			statsFilename: path.join(__dirname, "..", "..", "docs", "bundles_report_stats.json"),
			openAnalyzer: false,
		}),
	]

}